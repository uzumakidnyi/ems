<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Member extends Authenticatable
{
    protected $table = 'members';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','avatar',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function social_account()
    {
        return $this->hasMany('App\Models\SocialAccount');
    }

    public function event()
    {
        return $this->belongsToMany('App\Models\Event')->withTimestamps()->withPivot('ticket_number','event_qr');
    }
}
