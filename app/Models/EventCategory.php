<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventCategory extends Model
{
    protected $table = 'event_category';

    protected $fillable = [
        'category_name'
    ];

    public function event()
    {
        return $this->belongsToMany('App\Models\Event');
    }
}
