<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialAccount extends Model
{
    protected $table = 'social_accounts';

    protected $fillable = ['member_id', 'provider_member_id', 'provider'];

    public function member()
    {
        return $this->belongsTo('App\Member');
    }
}
