<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventTime extends Model
{
    protected $table = 'event_times';

    protected $fillable = [
        'event_id','speaker_id','event_date','event_time','event_time_title','event_time_description'
    ];

    public function event()
    {
        return $this->belongsToMany('App\Models\Event');
    }

    public function speaker()
    {
        return $this->hasOne('App\Models\Speaker');
    }
}
