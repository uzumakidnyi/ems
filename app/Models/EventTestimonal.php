<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventTestimonal extends Model
{
    protected $table = 'event_testimonals';

    protected $fillable = [
        'event_id','testimonal_name','testimonal_position','testimonal_image','testimonal_description'
    ];

    public function event()
    {
        return $this->belongsToMany('App\Models\Event');
    }
}
