<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventSponsor extends Model
{
    protected $table = 'event_sponsors';

    protected $fillable = [
        'event_id','sponsor_name','sponsor_image','sponsor_website_link','sponsor_type'
    ];

    public function event()
    {
        return $this->belongsToMany('App\Models\Event');
    }
}
