<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Speaker extends Model
{
    protected $table = 'speakers';

    protected $fillable = [
      'event_id','event_time_id','speaker_name','speaker_position','speaker_image','topic_title','topic_description','facebook','twitter','linkin'
    ];

    public function event_time()
    {
        return $this->belongsTo('App\Models\EventTime');
    }

    public function event()
    {
        return $this->belongsToMany('App\Models\Event');
    }
}
