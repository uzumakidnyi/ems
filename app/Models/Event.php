<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = "events";

    protected $fillable = [
        'title',
        'sub_title',
        'city',
        'address',
        'lat',
        'long',
        'seat',
        'description',
        'status',
        'event_date',
        'contact_number',
        'complete',
        'feature'
    ];

    public function event_time()
    {
        return $this->hasMany('App\Models\EventTime');
    }

    public function speaker()
    {
        return $this->hasMany('App\Models\Speaker');
    }

    public function event_image()
    {
        return $this->hasMany('App\Models\EventImage');
    }

    public function event_sponsor()
    {
        return $this->hasMany('App\Models\EventSponsor');
    }

    public function event_category()
    {
        return $this->belongsToMany('App\Models\EventCategory')->withTimestamps();
    }

    public function event_testimonal()
    {
        return $this->hasMany('App\Models\EventTestimonal');
    }

    public function member(){
        return $this->belongsToMany('App\Member')->withTimestamps()->withPivot('ticket_number','event_qr');
    }

    public function get_nearby_place()
    {
//        $latitude = '';
//        $longitude = '';
//        $radius = '';
//        return $this->getTable()->select(
//            DB::raw("`id`, `lat` , `long` ,  ( 6371 * acos( cos( radians(?) ) * cos( radians( `lat` ) ) * cos( radians( `long` ) - radians(?) ) + sin( radians(?) ) * sin( radians( `lat` ) ) )) AS `distance`"))
//            ->groupBy('name')
//            ->havingRaw("distance < ?")
//            ->orderBy("distance")
//            ->setBindings([$latitude, $longitude, $latitude, $radius])
//            ->get();

        return $this->all();
//        return DB::select('select id,title,address,contact_number,event_date , 3959 * acos( cos( radians(37) ) * cos( radians( lat ) ) * cos( radians( long ) - radians(-122) ) + sin( radians(37) ) * sin( radians( lat ) )  as distance from events ');
    }

    public function get_search_nearby_place($lat , $lng , $radius )
    {

    }
}
