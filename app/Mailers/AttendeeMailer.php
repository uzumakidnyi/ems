<?php
/**
 * Created by PhpStorm.
 * User: Nyi Nyi Lwin
 * Date: 7/27/2016
 * Time: 3:44 PM
 */

namespace App\Mailers;


use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class AttendeeMailer
{
    public function SendAttendeeInvite($member , $email )
    {

        Log::info("Sending invite to: " . $email );

        $data = [
            'attendee' => 'Hello World',
        ];

        Mail::queue('emails.send_event_invite', $data, function ($message) use ($member , $email ) {
            $message->to($email)->subject('Event Invitation !');
        });
    }
}