<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Mailers\AttendeeMailer;
use App\Member;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEventInviteEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $email;
    protected $member;

    /**
     * Create a new job instance.
     *
     * @param Member $member
     * @param $email
     */
    public function __construct(Member $member ,$email)
    {
        $this->member = $member ;
        $this->email = $email ;
    }

    /**
     * Execute the job.
     *
     * @param AttendeeMailer $attendeeMailer
     */
    public function handle(AttendeeMailer $attendeeMailer)
    {
        $attendeeMailer->SendAttendeeInvite( $this->member , $this->email );
    }
}
