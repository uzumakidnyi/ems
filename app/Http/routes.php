<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('aa','HomeController@sent_event_invite_email');

Route::group(['middleware' => 'checkinstall'], function () {

    Route::auth();

    Route::get('login/{provider}', 'Auth\SocialAuthController@redirect');
    Route::get('login/{provider}/callback', 'Auth\SocialAuthController@callback');

    //language Section
    Route::get('lang/{name}', function ($lang = 'en') {
        Session::forget('lang');
        Session::put('lang', $lang);
        return Redirect::back();
    });

    Route::get('/', 'HomeController@index');

    Route::get('home', 'HomeController@index');
    Route::get('blog', 'HomeController@blog');
    Route::get('event-list', 'HomeController@event_list');
    Route::get('event-detail/{name}', 'HomeController@event_detail');
    Route::get('contact-us', 'HomeController@contact_us');

    Route::post('complete_event','HomeController@complete_event');

    Route::post('checkemail', function (\Illuminate\Http\Request $request) {

        $member = \App\Member::whereEmail($request->email)->first();
        if (!$member) {
            return response()->json([
                'message' => 'We can\'t find a user with that e - mail address .',
            ], 500);
        }

        return response()->json([
            'message' => '1 record found',
        ], 200);

    });

    Route::group(['middleware' => 'auth', 'prefix' => 'member'], function () {
        Route::get('dashboard', 'MemberController@dashboard');
        Route::get('get_ticket/{event_id}', 'MemberController@get_ticket')->where('event_id', '[0-9]+');
        Route::get('get_qr_code/{event_id}', 'MemberController@get_qr_code')->where('event_id', '[0-9]+');
    });

});

//Installer Route
Route::group(['prefix' => 'install', 'middleware' => 'checkfile'], function () {
    //Welcome Page
    Route::get('/', 'InstallerController@welcome');
    Route::get('welcome', 'InstallerController@welcome');

    //Check ENV
    Route::get('environment', 'InstallerController@environment');
    Route::post('environmentSave', 'InstallerController@environmentSave');

    //Create User
    Route::get('user', 'InstallerController@user');
    Route::post('userSave', 'InstallerController@userSave');

    //Site Setting
    Route::get('setting', 'InstallerController@setting');
    Route::post('saveSetting', 'InstallerController@saveSetting');

    //Finished Install
    Route::get('finished', 'InstallerController@finished');

});

//Admin Route
Route::group(['namespace' => 'Admin'], function () {

    //login
    Route::get('letmein', 'LoginController@getLogin');
    Route::post('letmein', 'LoginController@postLogin');

    //logout
    Route::get('byebye', 'LoginController@getLogout');

    Route::group(['middleware' => 'admin'], function () {

        Route::get('documentation', 'DashboardController@documentation');

        //Setting
        Route::get('general_setting/email_setting', 'EmailController@index');
        Route::get('general_setting/site_setting', 'SettingController@site_setting');
        Route::get('general_setting/clear_cache', 'SettingController@clear_cache');
        Route::get('general_setting/clear_cache_data', 'SettingController@clear_cache_data');
        Route::get('general_setting/translation', 'SettingController@translation');
        Route::get('general_setting/removetranslation/{folder}', 'SettingController@removetranslation');

        Route::post('general_setting/addtranslation', 'SettingController@addtranslation');
        Route::post('general_setting/savetranslation', 'SettingController@savetranslation');
        Route::post('general_setting/save_site_setting', 'SettingController@save_site_setting');
        Route::post('general_setting/email_server_data', 'EmailController@email_server_data');

        //User Permission
        Route::get('module/manage_permission/{id}', 'ModuleController@manage_permission')->where('id', '[0-9]+');

        //Change Password
        Route::post('changePassword', 'AdminController@changePassword');
        Route::post('module/save_user_permission/{id}/{module_id}', 'ModuleController@save_user_permission')->where(['id' => '[0-9]+', 'module_id' => '[0-9]+']);

        Route::resource('admin', 'AdminController');
        Route::resource('dashboard', 'DashboardController');
        Route::resource('role', 'RoleController');
        Route::resource('module', 'ModuleController');
        Route::resource('blogs', 'BlogController');
        Route::resource('menu', 'MenuController');

        Route::get('menu/{id}/delete', 'MenuController@destroy');

        Route::resource('cms', 'CmsController');
        Route::get('cms/{theme}/page_list', 'CmsController@page_list');
        Route::get('cms/removetheme/{folder}', 'CmsController@removetheme');
        Route::post('cms/addtheme', 'CmsController@addtheme');

        Route::get('event/event_category', 'EventController@event_category');

        Route::get('event/get_event_category/{cat_id}', 'EventController@get_event_category')->where('cat_id', '[0-9]+');
        Route::get('event/get_speaker/{event_id}', 'EventController@get_speaker')->where('event_id', '[0-9]+');
        Route::get('event/get_event_time/{event_id}', 'EventController@get_event_time')->where('event_id', '[0-9]+');
        Route::get('event/get_testimonal/{event_id}', 'EventController@get_testimonal')->where('event_id', '[0-9]+');
        Route::get('event/get_event_sponsor/{event_id}', 'EventController@get_event_sponsor')->where('event_id', '[0-9]+');

        Route::get('event/make_event_feature/{event_id}', 'EventController@make_event_feature')->where('event_id', '[0-9]+');
        Route::get('event/make_active/{event_id}', 'EventController@make_active')->where('event_id', '[0-9]+');

        Route::post('event/add_new_event_category','EventController@add_new_event_category');
        Route::post('event/add_new_speaker/{event_id}', 'EventController@add_new_speaker')->where('event_id', '[0-9]+');
        Route::post('event/add_new_schedule/{event_id}', 'EventController@add_new_schedule')->where('event_id', '[0-9]+');
        Route::post('event/add_new_testimonal/{event_id}', 'EventController@add_new_testimonal')->where('event_id', '[0-9]+');
        Route::post('event/add_new_sponsor/{event_id}', 'EventController@add_new_sponsor')->where('event_id', '[0-9]+');

        Route::any('event/add_event_image/{event_id}', 'EventController@add_event_image')->where('event_id', '[0-9]+');

        Route::delete('event/delete_event_image/{event_id}', 'EventController@delete_event_image')->where('event_id', '[0-9]+');
        Route::delete('event/delete_event_time/{event_id}', 'EventController@delete_event_time')->where('event_id', '[0-9]+');
        Route::delete('event/delete_speaker/{event_id}', 'EventController@delete_speaker')->where('event_id', '[0-9]+');
        Route::delete('event/delete_testimonal/{event_id}', 'EventController@delete_testimonal')->where('event_id', '[0-9]+');
        Route::delete('event/delete_sponsor/{event_id}', 'EventController@delete_sponsor')->where('event_id', '[0-9]+');
        Route::delete('event/delete_event_category/{cat_id}', 'EventController@delete_event_category')->where('cat_id', '[0-9]+');

        Route::post('event/edit_speaker/{event_id}', 'EventController@edit_speaker')->where('event_id', '[0-9]+');
        Route::post('event/edit_schedule/{event_id}', 'EventController@edit_schedule')->where('event_id', '[0-9]+');
        Route::post('event/edit_testimonal/{event_id}', 'EventController@edit_testimonal')->where('event_id', '[0-9]+');
        Route::post('event/edit_sponsor/{event_id}', 'EventController@edit_sponsor')->where('event_id', '[0-9]+');

        Route::post('event/edit_catagory_name/{cat_id}', 'EventController@edit_catagory_name')->where('cat_id', '[0-9]+');

        Route::post('menu/saveorder', 'MenuController@saveorder');

        //include Created Module Route
        include('module_routes.php');
        include('log_viewer_routes.php');
    });

});
