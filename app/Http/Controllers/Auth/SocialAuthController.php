<?php

namespace App\Http\Controllers\Auth;

use App\Library\SocialAccountService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthController extends Controller
{
    /**
     * SocialAuthController constructor.
     * @param $provider
     * @return
     * @internal param SocialAccountService $service
     */

    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function callback(SocialAccountService $service ,$provider)
    {
        // Important change from previous post is that I'm now passing
        // whole driver, not only the user. So no more ->user() part
        $user = $service->createOrGetUser(Socialite::driver($provider));

        Auth::login($user);

        return redirect()->back();
    }
}
