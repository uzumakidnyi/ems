<?php

namespace App\Http\Controllers;

use App\Member;
use App\Models\Event;
use App\Models\SiteSettings;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class MemberController extends Controller
{

    protected $siteSettings;
    protected $member;

    /**
     * MemberController constructor.
     * @param SiteSettings $siteSettings
     * @param Member $member
     */
    public function __construct(SiteSettings $siteSettings, Member $member)
    {
        parent::__construct();
        $this->siteSettings = $siteSettings::find(1);
        $this->member = $member->find(Auth::user()->id);
    }

    public function dashboard()
    {



        $theme = $this->siteSettings;
        $member = $this->member->event()->get();
        return view('theme.' .  $theme->theme . '.member.dashboard', compact('theme','member'));
    }

    public function get_ticket(Event $event)
    {

        if($event->seat > 0 ) {
            $ticket_number = 'EV' . self::generateCode($event->id);
            $text = 'Event Title : '.$event->title . ',Ticket Number : '.$ticket_number . ',' . $this->member->name . ',' . $this->member->email;
            $event_qr = 'data:image/png;base64,' . base64_encode(\QrCode::format('png')->size(400)->generate($text));

            $this->member->event()->attach($event->id, [
                'ticket_number' => $ticket_number,
                'event_qr' => $event_qr
            ]);

            $event->decrement('seat');
            return response()->json([
                'message' => 'Ticket Complete',
            ], 200);

        }

        return response()->json([
            'message' => 'No Seat Available',
        ], 500);
    }

    public function get_qr_code(Event $event)
    {
        $image = $event->member()->where('id',$this->member->id)->first() ;
        return $image->pivot->event_qr ;
    }


    public function generateCode($event_id)
    {
        $today_date = date("dm");
        $today_time = date("Hs");
        $rand = mt_rand(12010, 98710);
        $rand2 = mt_rand(10, 90);
        $ticket_id = $rand . $event_id . '-' . $rand2 . $today_date . $today_time;
        return $ticket_id;
    }


}
