<?php

namespace App\Http\Controllers;

use App\Jobs\SendEventInviteEmail;
use App\Models\CmsPage;
use App\Models\Event;
use App\Models\EventCategory;
use App\Models\SiteSettings;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use SoapBox\Formatter\Formatter;

class HomeController extends Controller
{
    protected $siteSettings;
    protected $cmsPage;
    protected $eventCategory;
    protected $event;

    /**
     * HomeController constructor.
     * @param SiteSettings $siteSettings
     * @param CmsPage $cmsPage
     * @param Event $event
     * @param EventCategory $eventCategory
     */
    public function __construct(SiteSettings $siteSettings, CmsPage $cmsPage, Event $event, EventCategory $eventCategory)
    {
        parent::__construct();
        $this->siteSettings = $siteSettings->find(1);
        $this->cmsPage = $cmsPage;
        $this->event = $event;
        $this->eventCategory = $eventCategory;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $theme = $this->siteSettings;
        $page = $this->cmsPage->getFrontPage($request->path());
        $event = $this->event->orderBy('event_date', 'asc')->where('complete','!=','1')->take(5)->get();

        return view('theme.' . $theme->theme . '.pages.' . $page->filename, compact('theme','event'));
    }

    public function blog(Request $request)
    {
        $theme = $this->siteSettings;
        $page = $this->cmsPage->getFrontPage($request->path());
        $place = self::get_near_events();
        return view('theme.' . $theme->theme . '.pages.event_map', compact('theme', 'place'));
    }

    public function contact_us(Request $request)
    {
        $theme = $this->siteSettings;
        $page = $this->cmsPage->getFrontPage($request->path());
        return view('theme.' . $theme->theme . '.pages.' . $page->filename, compact('theme'));
    }

    public function event_list(Request $request)
    {
        $theme = $this->siteSettings;
        $page = $this->cmsPage->getFrontPage($request->path());
        if ($request->get('category') && $request->get('id')) {
            $event = $this->event->whereHas('event_category', function ($query) use ($request) {
                $query->where('event_category_id', $request->get('id'));
            })->orderBy('event_date', 'asc')->where('status', '1')->get();
        } else {
            $event = $this->event->orderBy('event_date', 'asc')->where('status', '1')->get();
        }
        $categories = $this->eventCategory->all();
        return view('theme.' . $theme->theme . '.pages.' . $page->filename, compact('theme', 'event', 'categories'));
    }

    public function event_detail(Request $request, $name)
    {
        $event = $this->event->find($request->get('id'));

        if ($name != @Str::slug($event->title))
            abort(404);

        $theme = $this->siteSettings;
        $page = $this->cmsPage->getFrontPage($request->path());
        return view('theme.' . $theme->theme . '.pages.event_detail', compact('theme', 'event'));
    }

    public function get_near_events()
    {
        $near_events = $this->event->get_nearby_place();
        return response()->json($near_events);
    }

    public function sent_event_invite_email(Request $request)
    {
        $member = Auth::user();
        $this->dispatch(new SendEventInviteEmail($member , 'nyinyilwin1992@hotmail.com'));
    }

    public function complete_event(Request $request){

        if($request->ajax()) {
            $update = $this->event->where('id', $request->input('id'))->update([
                'complete' => 1
            ]);

            if ($update) {
                return response()->json([
                    'message' => 'Complete!',
                ], 200);
            }
        }
        return response()->json([
            'message' => 'Something Went Wrong!',
        ], 500);

    }


}
