<?php

namespace App\Http\Controllers\Admin;

use App\Models\Event;
use App\Models\EventCategory;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use SiteHelper;

class EventController extends Controller
{
    protected $module = 'event';
    protected $permission = array();
    protected $access;
    protected $info;
    protected $event;
    protected $eventCategory;

    /**
     * EventController constructor.
     * @param Event $event
     * @param EventCategory $eventCategory
     */
    public function __construct(Event $event, EventCategory $eventCategory)
    {
        parent::__construct();
        $this->info = SiteHelper::moduleInfo($this->module);
        $this->access = SiteHelper::checkPermission($this->info->id);
        $this->event = $event;
        $this->eventCategory = $eventCategory;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ($this->access['view'] != '1')
            return view('admin.errors.403');

        $this->permission['permission'] = $this->access;
        $events = $this->event->all();
        return view('admin.event.index', compact('events'));
    }

    /**
     * @param Request $request
     * @param Event $event
     * @return mixed
     */
    public function make_active(Request $request, Event $event)
    {
        if ($request->get('type') == 'active') {
            $event->update(
                ['status' => 1]
            );
        } else {
            $event->update(
                ['status' => 0]
            );
        }
        return Redirect::back();
    }

    /**
     * @param Request $request
     * @param Event $event
     * @return mixed
     */
    public function make_event_feature(Request $request , Event $event)
    {
        if ($request->get('type') == 'feature') {
            $event->update(
                ['feature' => 1]
            );
        } else {
            $event->update(
                ['feature' => 0]
            );
        }
        return Redirect::back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $event_categories = $this->eventCategory->all();
        return view('admin.event.create',compact('event_categories'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->access['create'] != '1')
            return view('admin.errors.403');

        $event = $this->event->create($request->all());

        $this->SyncCategory($event , $request->input('event_category'));

        return Redirect::to('event' . '/' . $event->id);

    }

    /**
     * @param Request $request
     * @param Event $event
     * @return mixed
     */
    public function add_new_speaker(Request $request, Event $event)
    {
        $input = $request->all();
        if ($request->hasFile('speaker_image')) {
            $files = $request->file('speaker_image');
            $img = file_get_contents($files);
            $imagefile = 'data:image/png;base64,' . base64_encode($img);
            $input['speaker_image'] = $imagefile;
        }
        $event->speaker()->create($input);

        return Redirect::back();
    }

    /**
     * @param Request $request
     * @param Event $event
     * @return mixed
     */
    public function add_new_testimonal(Request $request, Event $event)
    {
        $input = $request->all();
        if ($request->hasFile('testimonal_image')) {
            $files = $request->file('testimonal_image');
            $img = file_get_contents($files);
            $imagefile = 'data:image/png;base64,' . base64_encode($img);
            $input['testimonal_image'] = $imagefile;
        }
        $event->event_testimonal()->create($input);

        return Redirect::back();
    }

    /**
     * @param Request $request
     * @param Event $event
     * @return \Illuminate\Http\JsonResponse
     */
    public function add_event_image(Request $request, Event $event)
    {

        if ($request->hasFile('file')) {
            $files = $request->file('file');

            if (is_array($files)) {
                foreach ($files as $file) {
                    $img = file_get_contents($file);
                    $imagefile = 'data:image/png;base64,' . base64_encode($img);

                    $event->event_image()->create([
                        'event_image' => $imagefile,
                    ]);
                }
            } else {
                $img = file_get_contents($files);
                $imagefile = 'data:image/png;base64,' . base64_encode($img);

                $event->event_image()->create([
                    'event_image' => $imagefile,
                ]);
            }

        }

        $images = $event->event_image()->get();

        return response()->json($images);

    }

    /**
     * @param Request $request
     * @param Event $event
     * @return mixed
     */
    public function add_new_schedule(Request $request, Event $event)
    {
        $event->event_time()->create($request->all());
        return Redirect::back();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function add_new_event_category(Request $request)
    {
        $this->eventCategory->create($request->all());
        return Redirect::back();
    }

    public function add_new_sponsor(Request $request, Event $event)
    {
        $input = $request->all();
        if ($request->hasFile('sponsor_image')) {
            $files = $request->file('sponsor_image');
            $img = file_get_contents($files);
            $imagefile = 'data:image/png;base64,' . base64_encode($img);
            $input['sponsor_image'] = $imagefile;
        }
        $event->event_sponsor()->create($input);

        return Redirect::back();
    }

    /**
     * @param Request $request
     * @param Event $event
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_speaker(Request $request, Event $event)
    {
        $speaker = $event->speaker()->find($request->input('id'));

        return view('admin.event.speaker_edit', compact('speaker'));
    }

    /**
     * @param Request $request
     * @param Event $event
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_event_time(Request $request, Event $event)
    {
        $event_time = $event->event_time()->find($request->input('id'));

        return view('admin.event.event_time_edit', compact('event_time', 'event'));
    }

    /**
     * @param Request $request
     * @param Event $event
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_testimonal(Request $request, Event $event)
    {
        $testimonal = $event->event_testimonal()->find($request->input('id'));

        return view('admin.event.testimonal_edit', compact('testimonal'));
    }

    public function get_event_sponsor(Request $request, Event $event)
    {
        $sponsor = $event->event_sponsor()->find($request->input('id'));

        return view('admin.event.sponsor_edit', compact('sponsor'));
    }

    /**
     * @param EventCategory $eventCategory
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_event_category(EventCategory $eventCategory)
    {
        return view('admin.event.category_edit', compact('eventCategory'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function event_category()
    {
        if ($this->access['view'] != '1')
            return view('admin.errors.403');

        $this->permission['permission'] = $this->access;
        $event_categories = $this->eventCategory->all();
        return view('admin.event.category', compact('event_categories'));
    }

    /**
     * @param Request $request
     * @param Event $event
     */
    public function delete_event_image(Request $request, Event $event)
    {
        $event->event_image()->where('id', $request->input('id'))->delete();
    }

    /**
     * @param Request $request
     * @param Event $event
     */
    public function delete_event_time(Request $request, Event $event)
    {
        $event->event_time()->where('id', $request->input('id'))->delete();
    }

    /**
     * @param Request $request
     */
    public function delete_event_category(Request $request)
    {
        $this->eventCategory->where('id', $request->input('id'))->delete();
    }

    /**
     * @param Request $request
     * @param Event $event
     * @return mixed
     */
    public function edit_speaker(Request $request, Event $event)
    {
        $input = $request->except('_token');
        if ($request->hasFile('speaker_image')) {
            $files = $request->file('speaker_image');
            $img = file_get_contents($files);
            $imagefile = 'data:image/png;base64,' . base64_encode($img);
            $input['speaker_image'] = $imagefile;
        }

        $event->speaker()->where('id', $input['id'])->update($input);

        return Redirect::back();
    }

    /**
     * @param Request $request
     * @param Event $event
     * @return mixed
     */
    public function edit_schedule(Request $request, Event $event)
    {
        $input = $request->except('_token');
        $event->event_time()->where('id', $input['id'])->update($input);
        return Redirect::back();
    }

    /**
     * @param Request $request
     * @param Event $event
     * @return mixed
     */
    public function edit_testimonal(Request $request, Event $event)
    {
        $input = $request->except('_token');
        if ($request->hasFile('testimonal_image')) {
            $files = $request->file('testimonal_image');
            $img = file_get_contents($files);
            $imagefile = 'data:image/png;base64,' . base64_encode($img);
            $input['testimonal_image'] = $imagefile;
        }

        $event->event_testimonal()->where('id', $input['id'])->update($input);

        return Redirect::back();
    }

    public function edit_sponsor(Request $request, Event $event)
    {
        $input = $request->except('_token');
        if ($request->hasFile('sponsor_image')) {
            $files = $request->file('sponsor_image');
            $img = file_get_contents($files);
            $imagefile = 'data:image/png;base64,' . base64_encode($img);
            $input['sponsor_image'] = $imagefile;
        }

        $event->event_sponsor()->where('id', $input['id'])->update($input);

        return Redirect::back();
    }

    /**
     * @param Request $request
     * @param EventCategory $eventCategory
     * @return mixed
     */
    public function edit_catagory_name(Request $request, EventCategory $eventCategory)
    {
        $input = $request->except('_token');
        $eventCategory->where('id', $input['id'])->update($input);
        return Redirect::back();
    }

    /**
     * Delete Event Speaker
     *
     * @param Request $request
     * @param Event $event
     */
    public function delete_speaker(Request $request, Event $event)
    {
        $event->speaker()->where('id', $request->input('id'))->delete();
    }

    /**
     * Delete Event Testimonal
     *
     * @param Request $request
     * @param Event $event
     */
    public function delete_testimonal(Request $request, Event $event)
    {
        $event->event_testimonal()->where('id', $request->input('id'))->delete();
    }

    public function delete_sponsor(Request $request, Event $event)
    {
        $event->event_sponsor()->where('id', $request->input('id'))->delete();
    }

    /**
     * Display the specified resource.
     *
     * @param Event $event
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(Event $event)
    {
        $event_categories = $this->eventCategory->all();
        $category = $event->event_category()->orderBy('id','asc')->pluck('id')->toArray();
        return view('admin.event.view', compact('event','event_categories','category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.event.update');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Event $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        if ($this->access['update'] != '1')
            return view('admin.errors.403');

        $event->update($request->all());
        $this->SyncCategory($event , $request->input('event_category'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->access['delete'] != '1')
            return view('admin.errors.403');

    }

    /**
     * Sync Event Category
     *
     * @param $event
     * @param array $categories
     */
    private function SyncCategory($event , array $categories)
    {
        $event->event_category()->sync($categories);
    }

}