<?php
/**
 * Created by PhpStorm.
 * User: Nyi Nyi Lwin
 * Date: 7/13/2016
 * Time: 7:21 PM
 */

namespace app\Library;

use App\Member;
use App\Models\SocialAccount;
use Laravel\Socialite\Contracts\Provider;

class SocialAccountService
{

    /**
     * @param Provider $provider
     * @return static
     */
    public function createOrGetUser(Provider $provider)
    {

        $providerUser = $provider->user();
        $providerName = class_basename($provider);

        $account = SocialAccount::where('provider' ,'=',$providerName)
            ->where('provider_member_id','=',$providerUser->getId())
            ->first();

        if ($account) {
            return $account->member;
        } else {

            $account = new SocialAccount([
                'provider_member_id' => $providerUser->getId(),
                'provider' => $providerName
            ]);

            $user = Member::where('email' , $providerUser->getEmail())->first();

            if (!$user) {

                $user = Member::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->getName(),
                    'avatar' => $providerUser->getAvatar(),
                ]);
            }

            $account->member()->associate($user);
            $account->save();

            return $user;

        }

    }
}