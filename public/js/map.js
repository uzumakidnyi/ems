// Google Map API Standard Code

var map;
var marker;

var geocoder;

var region = 'mm';

var markers = new Array();

$(document).ready(function(){

    var address = geoip_city()+', '+geoip_country_name();
    var lat = "";
    var lng = "";
    var location = "";



    if($('#add_edit_body #map_canvas').length) {

        geocoder = new google.maps.Geocoder();

        geocoder.geocode( {'address':address,'region':region}, function(results, status) {

            if(status == google.maps.GeocoderStatus.OK) {

                var lat = 16.798703652839684;
                var lng = 96.14947007373053;
                //var lat = results[0].geometry.location.lat();
                //var lng = results[0].geometry.location.lng();
                var location = "Shwedagon Pagoda, Yangon, Myanmar (Burma)";

                var gmap_marker = false;
                if($('#latitude').length) {

                    val = $('#latitude').val()*1;

                    if(val != '' && !isNaN(val)) {
                        lat = val;
                        gmap_marker = true;
                    }

                }


                if($('#longitude').length) {

                    val = $('#longitude').val()*1;

                    if(val != '' && !isNaN(val)) {
                        lng = val;
                    }
                }

                geocoder = new google.maps.Geocoder();

                var latlng = new google.maps.LatLng(lat,lng);

                var myOptions = {
                    zoom: 12,
                    center: latlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);


                if(gmap_marker) {

                    /*var marker = new google.maps.Marker({
                     map: map,
                     position: latlng
                     });*/
                }


                marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    title: 'Drag Me',
                    draggable: true
                });

                marker.addListener('click', toggleBounce);



                var infowindow = new google.maps.InfoWindow({
                    maxWidth: "300",
                    content: 'Drag Me'
                });

                infowindow.open(map,marker);

                google.maps.event.addListener(map, 'zoom_changed', function() {
                    $('input[name=zoom]').val(map.getZoom());
                });


                //google.maps.event.addListener(map, 'click', function() {
                //	$('input[name=zoom]').val(map.getZoom());
                //	$('input[name=longitude]').val(event.latLng.lng()); // lat
                //	$('input[name=latitude]').val(event.latLng.lat()); // long
                //});

                google.maps.event.addListener(marker, 'drag', function(event) {
                    $('input[name=longitude]').val(event.latLng.lng()); // lat
                    $('input[name=latitude]').val(event.latLng.lat()); // long
                });

                google.maps.event.addListener(marker, 'dragend', function(event) {
                    //console.log(event);
                    $('input[name=longitude]').val(event.latLng.lng()); // lat
                    $('input[name=latitude]').val(event.latLng.lat()); // long

                    initialize( $('input[name=latitude]').val(),$('input[name=longitude]').val());

                    var latitude = document.getElementById('latitude').value;
                    var longitude = document.getElementById('longitude').value;
                    var latLng = new google.maps.LatLng(latitude,longitude);
                    geocoder.geocode({
                            latLng: latLng
                        },
                        function(responses)
                        {
                            if (responses && responses.length > 0)
                            {
                                $('input[name=address]').val(responses[0].formatted_address);

                                $('input[name=place_id]').val( responses[0].place_id );

                                //$('input[name=telephone]').val( responses[0].formatted_phone_number );

                                var service = new google.maps.places.PlacesService(map);

                                service.getDetails({
                                    placeId: responses[0].place_id
                                }, function(place, status ) {
                                    if (status === google.maps.places.PlacesServiceStatus.OK) {
                                        $('input[name=name]').val( place.name );

                                    }
                                });
                            }
                            else
                            {
                                alert('Not getting Any address for given latitude and longitude.');
                            }
                        }
                    );
                });

            }});


    }

    if($('#add_edit_body #address').length) {

        $('#add_edit_body #address').blur(function(){

            var address = $(this).val();

            if(address != '') {

                get_coordinate(address,region);
            }
        });
    }

});


/**
 * Get address location
 */

function get_coordinate(address, region) {


    if(region==null || region == '' || region == 'undefined') {
        region = 'us';
    }

    if(address != '') {
        $('#ajax_msg').html('<p>Loading location</p>');

        geocoder.geocode( {'address':address,'region':region}, function(results, status) {

            if(status == google.maps.GeocoderStatus.OK) {
                $('#ajax_msg').html('<p></p>');
                // populate form field with geo location
                $('#latitude').val( results[0].geometry.location.lat() );
                $('#longitude').val( results[0].geometry.location.lng() );

                map.setZoom(10);

                map.setCenter(results[0].geometry.location);

                // Google Map Marker

                marker.setPosition(results[0].geometry.location);

            } else {

                $('#ajax_msg').html('<p>Google map geocoder failed: '+status+'</p>');
            }
        });
    }
}

function toggleBounce() {
    if (marker.getAnimation() !== null) {
        marker.setAnimation(null);
    } else {
        marker.setAnimation(google.maps.Animation.BOUNCE);
    }
}

function createPhotoMarker(place) {
    var photos = place.photos;
    if (!photos) {
        return;
    }

    var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location,
        title: place.name,
        icon: photos[0].getUrl({'maxWidth': 35, 'maxHeight': 35})
    });
}
