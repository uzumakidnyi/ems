<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('sub_title');
            $table->string('city');
            $table->string('address');
            $table->float('lat');
            $table->float('long');
            $table->integer('seat');
            $table->string('contact_number');
            $table->text('description');
            $table->date('event_date');
            $table->integer('status');
            $table->integer('complete');
            $table->integer('feature');
            $table->timestamps();
        });

        Schema::create('event_member', function (Blueprint $table) {
            $table->integer('event_id')->unsigned()->index();
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');

            $table->integer('member_id')->unsigned()->index();
            $table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');

            $table->string('ticket_number');
            $table->longText('event_qr');
            $table->integer('complete');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
        Schema::drop('event_member');
    }
}
