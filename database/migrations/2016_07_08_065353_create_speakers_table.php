<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpeakersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('speakers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id');
            $table->integer('event_time_id');
            $table->string('speaker_name');
            $table->string('speaker_position');
            $table->longText('speaker_image');
            $table->string('topic_title');
            $table->text('topic_description');
            $table->text('facebook');
            $table->text('twitter');
            $table->text('google_plus');
            $table->text('linkin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('speakers');
    }
}
