<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTestimonalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_testimonals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id');
            $table->string('testimonal_name');
            $table->string('testimonal_position');
            $table->longText('testimonal_image');
            $table->text('testimonal_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event_testimonals');
    }
}
