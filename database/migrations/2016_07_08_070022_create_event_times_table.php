<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_times', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id');
            $table->integer('speaker_id');
            $table->date('event_date');
            $table->time('event_time');
            $table->string('event_time_title');
            $table->string('event_time_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event_times');
    }
}
