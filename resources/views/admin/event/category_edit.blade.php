
{!! Form::open(array('url'=>'event/edit_catagory_name'.'/'.$eventCategory->id, 'files' => true ))  !!}
<input type="hidden" name="id" value="{{ $eventCategory->id }}">
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group form-md-line-input form-md-floating-label">
                <input type="text" class="form-control" id="category_name" name="category_name"
                       value="{{ $eventCategory->category_name }}" required>
                <label for="category_name">Category Name</label>
            </div>
        </div>
    </div>

</div>
<div class="modal-footer">
    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
    <button type="submit" class="btn green">Save Changes</button>
</div>
{!! Form::close() !!}
