{!! Form::open(array('url'=>'event/edit_testimonal'.'/'.$testimonal->event_id, 'files' => true ))  !!}
<input type="hidden" name="id" value="{{ $testimonal->id }}">
<div class="modal-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group form-md-line-input form-md-floating-label">
                <input type="text" class="form-control" id="testimonal_name" name="testimonal_name"
                       value="{{ $testimonal->testimonal_name }}" required>
                <label for="testimonal_name"> Name </label>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group form-md-line-input form-md-floating-label">
                <input type="text" class="form-control" id="testimonal_position"
                       name="testimonal_position"
                       value="{{ $testimonal->testimonal_position }}" required>
                <label for="testimonal_position"> Position</label>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group form-md-line-input form-md-floating-label">
                                                <textarea class="form-control" rows="3" name="testimonal_description"
                                                          placeholder="Testimonal Description">{{ $testimonal->testimonal_description }}</textarea>
                <div class="form-control-focus"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                    @if($testimonal->testimonal_image)
                        <img src="{{ $testimonal->testimonal_image }}" alt="" />
                    @else
                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                    @endif
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail"
                     style="max-width: 200px; max-height: 150px;"></div>
                <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="testimonal_image"> </span>
                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                        Remove </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
    <button type="submit" class="btn green">Submit</button>
</div>
{!! Form::close() !!}