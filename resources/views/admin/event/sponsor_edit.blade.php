
{!! Form::open(array('url'=>'event/edit_sponsor'.'/'.$sponsor->event_id, 'files' => true ))  !!}
<input type="hidden" name="id" value="{{ $sponsor->id }}">
<div class="modal-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group form-md-line-input form-md-floating-label">
                <input type="text" class="form-control" id="sponsor_name" name="sponsor_name"
                       value="{{ $sponsor->sponsor_name }}" required>
                <label for="sponsor_name">Sponsor Name</label>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group form-md-line-input form-md-floating-label">
                <input type="text" class="form-control" id="sponsor_website_link"
                       name="sponsor_website_link"
                       value="{{ $sponsor->sponsor_website_link }}">
                <label for="sponsor_website_link">Website Link( Optional )</label>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <select name="sponsor_type" class="form-control" id="">
                <option @if($sponsor->sponsor_type == 'silver') selected @endif value="silver">Silver Sponsor</option>
                <option @if($sponsor->sponsor_type == 'gold') selected @endif value="gold">Gold Sponsor</option>
                <option @if($sponsor->sponsor_type == 'platinum') selected @endif value="platinum">Platinum Sponsor</option>
            </select>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                    @if($sponsor->sponsor_image)
                        <img src="{{ $sponsor->sponsor_image }}" alt="" />
                    @else
                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                    @endif
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="sponsor_image"> </span>
                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="modal-footer">
    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
    <button type="submit" class="btn green">Save Changes</button>
</div>
{!! Form::close() !!}
