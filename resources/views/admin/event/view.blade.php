@extends('admin.layout.app')

@section('css')

    <link href="{{ asset('assets/global/plugins/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/global/plugins/dropzone/basic.min.css') }}" rel="stylesheet" type="text/css"/>

    <link href="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet"
          type="text/css"/>z
    <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}"
          rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}"
          rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}"
          rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/global/plugins/clockface/css/clockface.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/global/plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/global/plugins/socicon/socicon.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}"
          rel="stylesheet"
          type="text/css"/>
    <link href="../assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css"/>
@endsection

@section('qr')
    {!! Html::script('qr/llqrcode.js') !!}
    {!! Html::script('qr/webqr.js') !!}
@stop

@section('content')

    <style>
        #map {
            height: 100%;
        }

        .controls {
            margin-top: 10px;
            border: 1px solid transparent;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            height: 32px;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        }

        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 300px;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }

        .pac-container {
            font-family: Roboto;
        }

        #type-selector {
            color: #fff;
            background-color: #4d90fe;
            padding: 5px 11px 0px 11px;
        }

        #type-selector label {
            font-family: Roboto;
            font-size: 13px;
            font-weight: 300;
        }

        #target {
            width: 345px;
        }

        .mt-card-social > ul {
            padding: 0;
            margin-bottom: 10px;
        }

        .mt-card-social > ul > li {
            list-style: none;
            display: inline-block;
            margin: 0 3px;
        }

        .mt-card-social > ul > li > a {
            color: #000;
            font-size: 18px;
        }

        img{
            border:0;
        }
        #main{
            margin: 15px auto;
            background:white;
            overflow: auto;
            width: 100%;
        }
        #header{
            background:white;
            margin-bottom:15px;
        }
        #mainbody{
            background: white;
            width:100%;
            display:none;
        }
        #footer{
            background:white;
        }
        #v{
            width:320px;
            height:240px;
        }
        #qr-canvas{
            display:none;
        }
        #qrfile{
            width:320px;
            height:240px;
        }
        #mp1{
            text-align:center;
            font-size:35px;
        }
        #imghelp{
            position:relative;
            left:0px;
            top:-160px;
            z-index:100;
            font:18px arial,sans-serif;
            background:#f0f0f0;
            margin-left:35px;
            margin-right:35px;
            padding-top:10px;
            padding-bottom:10px;
            border-radius:20px;
        }
        .selector{
            margin:0;
            padding:0;
            cursor:pointer;
            margin-bottom:-5px;
        }
        #outdiv
        {
            width:320px;
            height:240px;
            border: solid;
            border-width: 3px 3px 3px 3px;
        }
        #result{
            border: solid;
            border-width: 1px 1px 1px 1px;
            padding:20px;
            width:70%;
        }
    </style>

    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Tabs, Accordions & Navs
                        <small>tabs, accordions & navbars</small>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
                <!-- BEGIN PAGE TOOLBAR -->
                <div class="page-toolbar">
                    <!-- BEGIN THEME PANEL -->
                    <div class="btn-group btn-theme-panel">
                        <a href="javascript:;" class="btn dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-settings"></i>
                        </a>
                        <div class="dropdown-menu theme-panel pull-right dropdown-custom hold-on-click">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <h3>THEME</h3>
                                    <ul class="theme-colors">
                                        <li class="theme-color theme-color-default" data-theme="default">
                                            <span class="theme-color-view"></span>
                                            <span class="theme-color-name">Dark Header</span>
                                        </li>
                                        <li class="theme-color theme-color-light active" data-theme="light">
                                            <span class="theme-color-view"></span>
                                            <span class="theme-color-name">Light Header</span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-12 seperator">
                                    <h3>LAYOUT</h3>
                                    <ul class="theme-settings">
                                        <li> Layout
                                            <select class="layout-option form-control input-small input-sm">
                                                <option value="fluid" selected="selected">Fluid</option>
                                                <option value="boxed">Boxed</option>
                                            </select>
                                        </li>
                                        <li> Header
                                            <select class="page-header-option form-control input-small input-sm">
                                                <option value="fixed" selected="selected">Fixed</option>
                                                <option value="default">Default</option>
                                            </select>
                                        </li>
                                        <li> Top Dropdowns
                                            <select class="page-header-top-dropdown-style-option form-control input-small input-sm">
                                                <option value="light">Light</option>
                                                <option value="dark" selected="selected">Dark</option>
                                            </select>
                                        </li>
                                        <li> Sidebar Mode
                                            <select class="sidebar-option form-control input-small input-sm">
                                                <option value="fixed">Fixed</option>
                                                <option value="default" selected="selected">Default</option>
                                            </select>
                                        </li>
                                        <li> Sidebar Menu
                                            <select class="sidebar-menu-option form-control input-small input-sm">
                                                <option value="accordion" selected="selected">Accordion</option>
                                                <option value="hover">Hover</option>
                                            </select>
                                        </li>
                                        <li> Sidebar Position
                                            <select class="sidebar-pos-option form-control input-small input-sm">
                                                <option value="left" selected="selected">Left</option>
                                                <option value="right">Right</option>
                                            </select>
                                        </li>
                                        <li> Footer
                                            <select class="page-footer-option form-control input-small input-sm">
                                                <option value="fixed">Fixed</option>
                                                <option value="default" selected="selected">Default</option>
                                            </select>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END THEME PANEL -->
                </div>
                <!-- END PAGE TOOLBAR -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="#">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span class="active">UI Features</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN TAB PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title tabbable-line">
                            <div class="caption">
                                <i class="icon-share font-dark"></i>
                                <span class="caption-subject font-dark bold uppercase">Event Information</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <ul class="nav nav-tabs tabs-left">
                                        <li class="active">
                                            <a href="#event_detail" data-toggle="tab">Event Detail </a>
                                        </li>
                                        <li>
                                            <a href="#speaker_list" data-toggle="tab"> Speaker List </a>
                                        </li>
                                        <li>
                                            <a href="#event_time" data-toggle="tab"> Event Time </a>
                                        </li>
                                        <li>
                                            <a href="#event_image" data-toggle="tab"> Event Image </a>
                                        </li>
                                        <li>
                                            <a href="#event_register_list" data-toggle="tab"> Attendees </a>
                                        </li>
                                        <li>
                                            <a href="#event_sponsor" data-toggle="tab"> Event Sponsor </a>
                                        </li>
                                        <li>
                                            <a href="#event_testimonal" data-toggle="tab"> Event Testimonals </a>
                                        </li>
                                        <li>
                                            <a href="#event_checkin" data-toggle="tab"> Event CheckIn </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-10">
                                    <div class="tab-content">
                                        <div class="tab-pane" id="event_checkin">
                                            <table class="tsel" border="0" width="100%">
                                                <tr>
                                                    <td valign="top" align="center" width="50%">
                                                        <table class="tsel" border="0">
                                                            <tr>
                                                                <td><img class="selector" id="webcamimg" src="vid.png" onclick="setwebcam()" alt="Camera" align="left" /></td>
                                                                <td><img class="selector" id="qrimg" src="cam.png" onclick="setimg()" align="right"/></td></tr>
                                                            <tr><td colspan="2" align="center">
                                                                    <div id="outdiv">
                                                                    </div></td></tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr><td colspan="3" align="center">
                                                        <img src="down.png"/>
                                                    </td></tr>
                                                <tr><td colspan="3" align="center">
                                                        <div id="result"></div>
                                                    </td></tr>
                                            </table>
                                            <canvas id="qr-canvas" width="800" height="600"></canvas>
                                        </div>
                                        <div class="tab-pane" id="event_testimonal">
                                            <div class="row">
                                                <div class="btn-group" style="float: right;padding-right: 30px;">
                                                    <a class="btn  red btn-outline btn-circle btn-sm"
                                                       data-toggle="modal"
                                                       href="#add_new_testimonal">
                                                        <i class="fa fa-hourglass"></i> Add Testimonal
                                                    </a>
                                                </div>
                                            </div>
                                            <br>
                                            <br>
                                            <div class="mt-comments">
                                                @foreach($event->event_testimonal as $testimonal)
                                                    <div class="mt-comment" id="testinomal_{{ $testimonal->id }}">
                                                        <div class="mt-comment-img">
                                                            @if( $testimonal->testimonal_image )
                                                                <img width="45" height="45"
                                                                     src="{{ $testimonal->testimonal_image }}"/>
                                                            @else
                                                                <img src="../assets/pages/media/users/avatar1.jpg"/>
                                                            @endif
                                                        </div>
                                                        <div class="mt-comment-body">
                                                            <div class="mt-comment-info">
                                                        <span class="mt-comment-author">{{ $testimonal->testimonal_name }}
                                                            - {{ $testimonal->testimonal_position }}</span>
                                                            </div>

                                                            <div class="mt-comment-text"> {{ $testimonal->testimonal_description }} </div>
                                                            <div class="mt-comment-details">
                                                                <ul class="mt-comment-actions">
                                                                    <li>
                                                                        <a href="#" class="btn btn-xs blue"
                                                                           style="color:#FFF"
                                                                           onclick="get_testimonal({{ $testimonal->id }})">Edit</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" class="btn btn-xs red"
                                                                           style="color:#FFF"
                                                                           onclick="delete_testimonal({{ $testimonal->id }})">Delete</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="event_sponsor">
                                            <div class="row">
                                                <div class="btn-group" style="float: right;padding-right: 30px;">
                                                    <a class="btn  red btn-outline btn-circle btn-sm"
                                                       data-toggle="modal"
                                                       href="#add_new_sponsor">
                                                        <i class="fa fa-hourglass"></i> Add Sponsor
                                                    </a>
                                                </div>
                                            </div>
                                            <br>
                                            <br>
                                            <div class="mt-comments">
                                                @foreach($event->event_sponsor()->orderBy('sponsor_type','asc')->get() as $sponsor)
                                                    <div class="mt-comment" id="sponsor_{{ $sponsor->id }}">
                                                        <div class="mt-comment-img">
                                                            @if( $sponsor->sponsor_image )
                                                                <img width="45" height="45"
                                                                     src="{{ $sponsor->sponsor_image }}"/>
                                                            @else
                                                                <img src="{{ public_path('assets/pages/media/users/avatar1.jpg') }}"/>
                                                            @endif
                                                        </div>
                                                        <div class="mt-comment-body">
                                                            <div class="mt-comment-info">
                                                                <span class="mt-comment-author">{{ $sponsor->sponsor_name }}</span>
                                                            </div>
                                                            <div class="mt-comment-text"> {{ $sponsor->sponsor_website_link }}
                                                                @if($sponsor->sponsor_type == 'silver')
                                                                    <a class="btn btn-xs" href="javascript:;">Silver
                                                                        Sponsor</a>
                                                                @elseif( $sponsor->sponsor_type == 'gold')
                                                                    <a class="btn btn-xs yellow" href="javascript:;">Gold
                                                                        Sponsor</a>
                                                                @else
                                                                    <a class="btn btn-xs grey" href="javascript:;">Platinum
                                                                        Sponsor</a>
                                                                @endif
                                                            </div>
                                                            <div class="mt-comment-details">
                                                                <ul class="mt-comment-actions">
                                                                    <li>
                                                                        <a href="#" class="btn btn-xs blue"
                                                                           style="color:#FFF"
                                                                           onclick="get_sponsor({{ $sponsor->id }})">Edit</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" class="btn btn-xs red"
                                                                           style="color:#FFF"
                                                                           onclick="delete_sponsor({{ $sponsor->id }})">Delete</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="event_register_list">
                                            <div class="portlet-body">
                                                <table class="table table-striped table-bordered table-hover dt-responsive"
                                                       width="100%"
                                                       id="sample_1">
                                                    <thead>
                                                    <tr>
                                                        <th class="all" style="width: 35%">Ticket Number</th>
                                                        <th class="min-phone-l">Member Name</th>
                                                        <th class="min-tablet">Email</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    @foreach($event->member as $module)
                                                        <tr>
                                                            <td class="uk-text-center" style="width: 35%">
                                                                {{ $module->pivot->ticket_number }}
                                                            </td>
                                                            <td class="uk-text-center">
                                                                {{ $module->name }}
                                                            </td>
                                                            <td class="uk-text-center">
                                                                {{ $module->email }}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="event_image">
                                            {!! Form::open(array('url'=>'event/add_event_image'.'/'.$event->id, 'class'=>'dropzone dropzone-file-area' ,'files' => true, 'id'=>'my-dropzone'))  !!}
                                            <h3 class="sbold">Drop files here or click to upload</h3>
                                            {!! Form::close() !!}
                                        </div>
                                        <div class="tab-pane" id="event_time">
                                            <div class="row">
                                                <div class="btn-group" style="float: right;padding-right: 30px;">
                                                    <a class="btn  red btn-outline btn-circle btn-sm"
                                                       data-toggle="modal"
                                                       href="#add_new_time">
                                                        <i class="fa fa-hourglass"></i> Add Schedule
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-10 col-md-offset-1">
                                                    <div class="cd-horizontal-timeline mt-timeline-horizontal">
                                                        <div class="timeline">
                                                            <div class="events-wrapper">
                                                                <div class="events">
                                                                    <ol>
                                                                        <?php $i = 1 ?>
                                                                        @foreach($event->event_time()->groupBy('event_date')->get() as $key => $date)
                                                                            <li>
                                                                                <a href="#0"
                                                                                   data-date="{{ date('d/m/Y' , strtotime($date->event_date)) }}"
                                                                                   class="border-after-red bg-after-red @if($key == 0 ) selected @endif">Day {{ $i }}</a>
                                                                            </li>
                                                                            <?php $i++ ?>
                                                                        @endforeach
                                                                    </ol>
                                                                    <span class="filling-line bg-red"
                                                                          aria-hidden="true"></span>
                                                                </div>
                                                                <!-- .events -->
                                                            </div>
                                                            <!-- .events-wrapper -->
                                                            <ul class="cd-timeline-navigation mt-ht-nav-icon">
                                                                <li>
                                                                    <a href="#0"
                                                                       class="prev inactive btn btn-outline red md-skip">
                                                                        <i class="fa fa-chevron-left"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#0"
                                                                       class="next btn btn-outline red md-skip">
                                                                        <i class="fa fa-chevron-right"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                            <!-- .cd-timeline-navigation -->
                                                        </div>
                                                        <!-- .timeline -->
                                                        <div class="events-content" style="overflow: hidden">
                                                            <ol>
                                                                @foreach($event->event_time()->groupBy('event_date')->get() as $key => $date)
                                                                    <li class="@if($key == 0 ) selected @endif"
                                                                        data-date="{{ date('d/m/Y' , strtotime($date->event_date)) }}">
                                                                        <div class="timeline">
                                                                            @foreach($event->event_time()->where('event_date',$date->event_date)->orderBy('event_time','asc')->get() as $data )
                                                                                <div id="event_time_{{ $data->id }}"
                                                                                     class="timeline-item">
                                                                                    <div class="timeline-badge"
                                                                                         style="width: 125px;top: 27px;">
                                                                                        <a href="#" class=""
                                                                                           style="font-size: 1.5em;text-decoration: none;font-weight: 500;color: #000">
                                                                                            {{ date('g:i a' , strtotime($data->event_time)) }}
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="timeline-body">
                                                                                        <div class="timeline-body-arrow"></div>
                                                                                        <div class="timeline-body-head">
                                                                                            <div class="timeline-body-head-caption">
                                                                                                <a href="javascript:;"
                                                                                                   class="timeline-body-title font-blue-madison">
                                                                                                    {{ $data->event_time_title }}
                                                                                                </a>
                                                                                            </div>
                                                                                            <div class="timeline-body-head-actions">

                                                                                                <button class="btn btn-xs blue"
                                                                                                        onclick="get_event_time({{ $data->id }})"
                                                                                                        type="button">
                                                                                                    Edit
                                                                                                </button>
                                                                                                <button class="btn btn-xs red"
                                                                                                        onclick="delete_event_time({{ $data->id }})"
                                                                                                        type="button">
                                                                                                    Delete
                                                                                                </button>

                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="timeline-body-content">
                                                                                <span class="font-grey-cascade">
                                                                                    @if($data->speaker_id)
                                                                                        <p>{{ $event->speaker->find($data->speaker_id)->topic_description }}</p>
                                                                                    @else
                                                                                        <p>{{ $data->event_time_description }}</p>
                                                                                    @endif
                                                                                </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @endforeach
                                                                        </div>
                                                                    </li>
                                                                @endforeach
                                                            </ol>
                                                        </div>
                                                        <!-- .events-content -->
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                        </div>
                                        <div class="tab-pane" id="speaker_list">
                                            <div class="row">
                                                <div class="btn-group" style="float: right;padding-right: 30px;">
                                                    <a class="btn  red btn-outline btn-circle btn-sm"
                                                       data-toggle="modal"
                                                       href="#add_new_speaker">
                                                        <i class="fa fa-user"></i> Add New Speaker
                                                    </a>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="mt-comments">
                                                @foreach($event->speaker as $speaker)
                                                    <div class="mt-comment" id="speaker_{{ $speaker->id }}">
                                                        <div class="mt-comment-img">
                                                            @if($speaker->speaker_image)
                                                                <img width="45" height="45"
                                                                     src="{{ $speaker->speaker_image }}"/>
                                                            @else
                                                                <img src="../assets/pages/media/users/avatar1.jpg"/>
                                                            @endif
                                                        </div>
                                                        <div class="mt-comment-body">
                                                            <div class="mt-comment-info">
                                                        <span class="mt-comment-author">{{ $speaker->speaker_name }}
                                                            - {{ $speaker->speaker_position }}</span>
                                                            </div>
                                                            <div class="mt-comment-info">
                                                                <span class="mt-comment-author">Topic - {{ $speaker->topic_title }}</span>
                                                            </div>
                                                            <div class="mt-comment-text"> {{ $speaker->topic_description }} </div>
                                                            <div class="mt-comment-details">
                                                                <div class="mt-card-social">
                                                                    <ul>
                                                                        @if($speaker->facebook)
                                                                            <li>
                                                                                <a href="{{ $speaker->facebook }}"
                                                                                   target="_blank"
                                                                                   class="socicon-btn socicon-facebook tooltips"
                                                                                   data-original-title="Facebook"></a>
                                                                            </li>
                                                                        @endif
                                                                        @if($speaker->twitter)
                                                                            <li>
                                                                                <a href="{{ $speaker->twitter }}"
                                                                                   target="_blank"
                                                                                   class="socicon-btn socicon-twitter tooltips"
                                                                                   data-original-title="Twitter"></a>
                                                                            </li>
                                                                        @endif
                                                                        @if($speaker->google_plus)
                                                                            <li>
                                                                                <a href="{{ $speaker->google_plus }}"
                                                                                   target="_blank"
                                                                                   class="socicon-btn socicon-google tooltips"
                                                                                   data-original-title="Google"></a>
                                                                            </li>
                                                                        @endif
                                                                        @if($speaker->linkin)
                                                                            <li>
                                                                                <a href="{{ $speaker->linkin }}"
                                                                                   target="_blank"
                                                                                   class="socicon-btn socicon-linkedin tooltips"
                                                                                   data-original-title="Linkedin"></a>
                                                                            </li>
                                                                        @endif
                                                                    </ul>
                                                                </div>
                                                                <ul class="mt-comment-actions">
                                                                    <li>
                                                                        <a href="#" class="btn btn-xs blue"
                                                                           style="color:#FFF"
                                                                           onclick="get_speaker({{ $speaker->id }})">Edit</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" class="btn btn-xs red"
                                                                           style="color:#FFF"
                                                                           onclick="delete_speaker({{ $speaker->id }})">Delete</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="tab-pane active" id="event_detail">
                                            <form action="#" method="post" id="event_info" role="form">
                                                <div class="form-body">

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group form-md-line-input form-md-floating-label">
                                                                <input type="text" class="form-control" id="title"
                                                                       name="title"
                                                                       value="{{ $event->title }}" required>
                                                                <label for="title">Event Title</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group form-md-line-input form-md-floating-label">
                                                                <input type="text" class="form-control" id="sub_title"
                                                                       name="sub_title"
                                                                       value="{{ $event->sub_title }}" required>
                                                                <label for="sub_title">Event Sub-Title</label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group form-md-line-input form-md-floating-label">
                                                                <input type="text" class="form-control" id="city"
                                                                       value="{{ $event->city }}" name="city" required>
                                                                <label for="city">City</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group form-md-line-input form-md-floating-label">
                                                                <input type="text" class="form-control" id="title"
                                                                       name="address" value="{{ $event->address }}"
                                                                       required>
                                                                <label for="address">Event Address</label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group form-md-line-input form-md-floating-label input-group  date date-picker"
                                                                 data-date-format="yyyy-mm-dd"
                                                                 data-date-start-date="+0d">
                                                                <input type="text" name="event_date" id="event_date"
                                                                       value="{{ $event->event_date }}"
                                                                       class="form-control">
                                                        <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>
                                                                <label for="event_date">Event Date</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group form-md-line-input form-md-floating-label">
                                                                <input type="text" class="form-control" id="seat"
                                                                       name="seat"
                                                                       value="{{ $event->seat }}" required>
                                                                <label for="seat">Seat</label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group form-md-line-input form-md-floating-label">
                                                                <input type="text" class="form-control"
                                                                       id="contact_number"
                                                                       value="{{ $event->contact_number }}"
                                                                       name="contact_number" required>
                                                                <label for="contact_number">Contact Number</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group form-md-line-input">
                                                                <select id="multiple"
                                                                        class="form-control select2-multiple"
                                                                        name="event_category[]" multiple>
                                                                    @foreach( $event_categories as $event_category )
                                                                        <option @if( in_array($event_category->id , $category , TRUE) ) selected
                                                                                @endif value="{{ $event_category->id }}">{{ $event_category->category_name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                <label for="multiple">Event Category</label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group form-md-line-input form-md-floating-label">
                                                <textarea class="form-control" rows="3" name="description"
                                                          placeholder="Event Description">{{ $event->description }}</textarea>
                                                                <div class="form-control-focus"></div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="map-canvas"
                                                                 style="width:100%;height:400px;"></div>
                                                            <div id="ajax_msg"></div>
                                                        </div>
                                                    </div>

                                                    <input id="pac-input" class="controls" type="text"
                                                           placeholder="Search Place">

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group form-md-line-input form-md-floating-label">
                                                                <input type="text" class="form-control"
                                                                       id="input-latitude"
                                                                       placeholder="Latitude" value="{{ $event->lat }}"
                                                                       name="lat" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group form-md-line-input form-md-floating-label">
                                                                <input type="text" class="form-control"
                                                                       id="input-longitude"
                                                                       placeholder="Longitude"
                                                                       value="{{ $event->long }}"
                                                                       name="long"
                                                                       required>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="form-actions noborder">
                                                    <button type="button" onclick="detail_update({{ $event->id }})"
                                                            class="btn blue">Save Changes
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END TAB PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>

    <div class="modal fade draggable-modal" id="add_new_speaker" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add New Speaker</h4>
                </div>
                {!! Form::open(array('url'=>'event/add_new_speaker'.'/'.$event->id, 'files' => true ))  !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="speaker_name" name="speaker_name"
                                       value="" required>
                                <label for="speaker_name">Speaker's NAme</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="speaker_position"
                                       name="speaker_position"
                                       value="" required>
                                <label for="speaker_position">Speaker's Position</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="facebook" name="facebook">
                                <label for="facebook">Facebook</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="twitter" name="twitter">
                                <label for="twitter">Twitter</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="google_plus" name="google_plus">
                                <label for="google_plus">Google+</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="linkin" name="linkin">
                                <label for="linkin">Linkedin</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="topic_title" name="topic_title">
                                <label for="topic_title">Topic Title</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                                <textarea class="form-control" rows="3" name="topic_description"
                                                          placeholder="Topic Description"></textarea>
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail"
                                     style="max-width: 200px; max-height: 150px;"></div>
                                <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="speaker_image"> </span>
                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                                        Remove </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn green">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade draggable-modal" id="add_new_testimonal" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add New Testimonal</h4>
                </div>
                {!! Form::open(array('url'=>'event/add_new_testimonal'.'/'.$event->id, 'files' => true ))  !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="testimonal_name" name="testimonal_name"
                                       value="" required>
                                <label for="testimonal_name"> Name </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="testimonal_position"
                                       name="testimonal_position"
                                       value="" required>
                                <label for="testimonal_position"> Position</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                                <textarea class="form-control" rows="3" name="testimonal_description"
                                                          placeholder="Testimonal Description"></textarea>
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail"
                                     style="max-width: 200px; max-height: 150px;"></div>
                                <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="testimonal_image"> </span>
                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                                        Remove </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn green">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade draggable-modal" id="add_new_sponsor" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4>Add New Sponsor</h4>
                </div>
                {!! Form::open(array('url'=>'event/add_new_sponsor'.'/'.$event->id, 'files' => true ))  !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="sponsor_name" name="sponsor_name"
                                       value="" required>
                                <label for="sponsor_name">Sponsor Name</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="sponsor_website_link"
                                       name="sponsor_website_link"
                                       value="">
                                <label for="sponsor_website_link">Website Link( Optional )</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <select name="sponsor_type" class="form-control" id="" required>
                                <option value="">Choose sponsor type</option>
                                <option value="silver">Silver Sponsor</option>
                                <option value="gold">Gold Sponsor</option>
                                <option value="platinum">Platinum Sponsor</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail"
                                     style="max-width: 200px; max-height: 150px;"></div>
                                <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="sponsor_image"> </span>
                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                                        Remove </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn green">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade draggable-modal" id="add_new_time" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add Schedule</h4>
                </div>
                {!! Form::open(array('url'=>'event/add_new_schedule'.'/'.$event->id, 'files' => true ))  !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label input-group  date date-picker"
                                 data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                                <input type="text" name="event_date" id="event_date"
                                       value="{{ $event->event_date }}"
                                       class="form-control">
                                                        <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>
                                <label for="event_date">Event Date</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label input-group">
                                <input type="text" name="event_time" id="event_time" placeholder="Event Time"
                                       class="form-control timepicker timepicker-24">
                                                        <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-clock-o"></i>
                                                            </button>
                                                        </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="event_time_title" name="event_time_title">
                                <label for="event_time_title">Schedule Title</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <select name="speaker_id" class="form-control" id="speaker_id">
                                    <option value="" selected>Speaker List ( Optional )</option>
                                    @foreach($event->speaker as $speaker)
                                        <option value="{{ $speaker->id }}">{{ $speaker->speaker_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                                <textarea class="form-control" rows="3" name="event_time_description"
                                                          placeholder="Schedule Description"></textarea>
                                <div class="form-control-focus"></div>
                            </div>
                            <span class="red">if u select speaker u dont need to write Schedule Description , Speaker's topic description will use </span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn green">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade draggable-modal" id="edit-modal" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body" id="edit-modal-content" style="z-index: 999;">

                </div>
            </div>
        </div>
    </div>

    <script>
        function map_pos() {
            initMap({{ $event->lat }},{{ $event->long }})
        }

        function detail_update(id) {

            var data = $('#event_info');

            $.ajax({
                type: "PUT",
                url: id,
                data: data.serialize(),
                success: function (data) {
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "1000",
                        "hideDuration": "1000",
                        "timeOut": "3000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    },
                            toastr.success("Update Complete !", "Event Details");
                },
                error: function (data) {

                }
            }, "json");

        }

        function initMap(mylat, mylong) {
            var mapOptions = {
                center: {lat: mylat, lng: mylong},
                zoom: 13
            };
            var map = new google.maps.Map(document.getElementById('map-canvas'),
                    mapOptions);

            var marker_position = {lat: mylat, lng: mylong};
            var input = /** @type {HTMLInputElement} */(
                    document.getElementById('pac-input'));

            var types = document.getElementById('type-selector');
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);

            var infowindow = new google.maps.InfoWindow();
            var marker = new google.maps.Marker({
                position: marker_position,
                draggable: true,
                map: map,
                anchorPoint: new google.maps.Point(0, -29)
            });


            google.maps.event.addListener(marker, "mouseup", function (event) {
                $('#input-latitude').val(this.position.lat());
                $('#input-longitude').val(this.position.lng());
            });

            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);
                }

                marker.setIcon(/** @type {google.maps.Icon} */({
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(35, 35)
                }));

                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                $('#input-latitude').val(place.geometry.location.lat());
                $('#input-longitude').val(place.geometry.location.lng());

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }

                $('input[name=address]').val(place.formatted_address);

                infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                infowindow.open(map, marker);
            });


            google.maps.event.addListener(marker, 'dragend', function () {

                $('#input-latitude').val(place.geometry.location.lat());
                $('#input-longitude').val(place.geometry.location.lng());

            });

        }

        function delete_speaker(id) {
            bootbox.dialog({
                message: "I am a custom dialog",
                title: "Are you sure!",
                buttons: {
                    main: {
                        label: "Cancel",
                        className: "grey"
                    },
                    danger: {
                        label: "Delete",
                        className: "red",
                        callback: function () {
                            $.ajax({
                                type: 'DELETE',
                                url: '{{ url('event/delete_speaker').'/'.$event->id  }}',
                                data: {
                                    'id': id
                                },
                                success: function (data) {
                                    toastr.options = {
                                        "closeButton": true,
                                        "debug": false,
                                        "positionClass": "toast-top-right",
                                        "onclick": null,
                                        "showDuration": "1000",
                                        "hideDuration": "1000",
                                        "timeOut": "3000",
                                        "extendedTimeOut": "1000",
                                        "showEasing": "swing",
                                        "hideEasing": "linear",
                                        "showMethod": "fadeIn",
                                        "hideMethod": "fadeOut"
                                    },
                                            toastr.success("Delete Complete !", "Speaker List");
                                    $('#speaker_' + id).remove();
                                },
                                error: function (data) {

                                }
                            }, "json");
                        }
                    }
                }
            });
        }

        function get_speaker(id) {
            MainModal('{{ url('event/get_speaker').'/'.$event->id  }}', 'Edit Speaker', id);
        }

        function get_event_time(id) {
            MainModal('{{ url('event/get_event_time').'/'.$event->id  }}', 'Edit Event', id);
        }

        function delete_event_time(id) {
            bootbox.dialog({
                message: "I am a custom dialog",
                title: "Are you sure!",
                buttons: {
                    main: {
                        label: "Cancel",
                        className: "grey"
                    },
                    danger: {
                        label: "Delete",
                        className: "red",
                        callback: function () {
                            $.ajax({
                                type: 'DELETE',
                                url: '{{ url('event/delete_event_time').'/'.$event->id  }}',
                                data: {
                                    'id': id
                                },
                                success: function (data) {
                                    toastr.options = {
                                        "closeButton": true,
                                        "debug": false,
                                        "positionClass": "toast-top-right",
                                        "onclick": null,
                                        "showDuration": "1000",
                                        "hideDuration": "1000",
                                        "timeOut": "3000",
                                        "extendedTimeOut": "1000",
                                        "showEasing": "swing",
                                        "hideEasing": "linear",
                                        "showMethod": "fadeIn",
                                        "hideMethod": "fadeOut"
                                    },
                                            toastr.success("Delete Complete !", "Event Time");
                                    $('#event_time_' + id).remove();
                                },
                                error: function (data) {

                                }
                            }, "json");
                        }
                    }
                }
            });
        }

        function get_testimonal(id) {
            MainModal('{{ url('event/get_testimonal').'/'.$event->id  }}', 'Edit Testimonal Detail', id);
        }

        function delete_testimonal(id) {
            bootbox.dialog({
                message: "I am a custom dialog",
                title: "Are you sure!",
                buttons: {
                    main: {
                        label: "Cancel",
                        className: "grey"
                    },
                    danger: {
                        label: "Delete",
                        className: "red",
                        callback: function () {
                            $.ajax({
                                type: 'DELETE',
                                url: '{{ url('event/delete_testimonal').'/'.$event->id  }}',
                                data: {
                                    'id': id
                                },
                                success: function (data) {
                                    toastr.options = {
                                        "closeButton": true,
                                        "debug": false,
                                        "positionClass": "toast-top-right",
                                        "onclick": null,
                                        "showDuration": "1000",
                                        "hideDuration": "1000",
                                        "timeOut": "3000",
                                        "extendedTimeOut": "1000",
                                        "showEasing": "swing",
                                        "hideEasing": "linear",
                                        "showMethod": "fadeIn",
                                        "hideMethod": "fadeOut"
                                    },
                                            toastr.success("Delete Complete !", "Event Testimonal");
                                    $('#testinomal_' + id).remove();
                                },
                                error: function (data) {

                                }
                            }, "json");
                        }
                    }
                }
            });
        }

        function get_sponsor(id) {
            MainModal('{{ url('event/get_event_sponsor').'/'.$event->id  }}', 'Edit Sponsor Detail', id);
        }

        function delete_sponsor(id) {
            bootbox.dialog({
                message: "I am a custom dialog",
                title: "Are you sure!",
                buttons: {
                    main: {
                        label: "Cancel",
                        className: "grey"
                    },
                    danger: {
                        label: "Delete",
                        className: "red",
                        callback: function () {
                            $.ajax({
                                type: 'DELETE',
                                url: '{{ url('event/delete_sponsor').'/'.$event->id  }}',
                                data: {
                                    'id': id
                                },
                                success: function (data) {
                                    toastr.options = {
                                        "closeButton": true,
                                        "debug": false,
                                        "positionClass": "toast-top-right",
                                        "onclick": null,
                                        "showDuration": "1000",
                                        "hideDuration": "1000",
                                        "timeOut": "3000",
                                        "extendedTimeOut": "1000",
                                        "showEasing": "swing",
                                        "hideEasing": "linear",
                                        "showMethod": "fadeIn",
                                        "hideMethod": "fadeOut"
                                    },
                                            toastr.success("Delete Complete !", "Event Sponsor");
                                    $('#sponsor_' + id).remove();
                                },
                                error: function (data) {

                                }
                            }, "json");
                        }
                    }
                }
            });
        }

        function MainModal(url, title, id) {
            $('#edit-modal-content').html(' ....Loading content , please wait ...');
            $('.modal-title').html(title);
            $('#edit-modal-content').load(url + '?' + 'id=' + id, function () {
            });
            $('#edit-modal').modal('show');
        }
    </script>
@endsection

@section('first_page_lvl_script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfXOeoxQMK3CXttc2l-qSz1chohv9UXN4&libraries=weather,geometry,visualization,places,drawing&callback=map_pos"
            async defer></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-toastr/toastr.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/clockface/js/clockface.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/dropzone/dropzone.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/horizontal-timeline/horozontal-timeline.min.js') }}"
            type="text/javascript"></script>

    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}"
            type="text/javascript"></script>
    <script src="../assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
@endsection

@section('second_page_lvl_script')
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/apps/scripts/timeline.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-responsive.min.js') }}"
            type="text/javascript"></script>

    <script>
        Dropzone.options.myDropzone = {
            init: function () {
                thisDropzone = this;

                $.ajax({
                    type: 'get',
                    url: '{{ url('event/add_event_image').'/'.$event->id  }}',
                    data: '',
                    dataType: 'json',
                    success: function (data) {
                        $.each(data, function (key, value) {

                            var mockFile = {name: value.event_image, id: value.id};

                            thisDropzone.options.addedfile.call(thisDropzone, mockFile);

                            thisDropzone.options.thumbnail.call(thisDropzone, mockFile, value.event_image);

                        });
                    }
                });

            },
            dictRemoveFileConfirmation: 'Are you sure!',

            addRemoveLinks: true,

            removedfile: function (file) {
                var id = file.id;
                $.ajax({
                    type: 'DELETE',
                    url: '{{ url('event/delete_event_image').'/'.$event->id  }}',
                    data: {
                        'id': id
                    },
                    dataType: 'json'
                });
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            }
        };
    </script>
    <script src="../assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
@endsection

