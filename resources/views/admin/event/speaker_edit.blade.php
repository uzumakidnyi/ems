
{!! Form::open(array('url'=>'event/edit_speaker'.'/'.$speaker->event_id, 'files' => true ))  !!}
                <input type="hidden" name="id" value="{{ $speaker->id }}">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="speaker_name" name="speaker_name"
                                       value="{{ $speaker->speaker_name }}" required>
                                <label for="speaker_name">Speaker's NAme</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="speaker_position"
                                       name="speaker_position"
                                       value="{{ $speaker->speaker_position }}" required>
                                <label for="speaker_position">Speaker's Position</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="facebook" value="{{ $speaker->facebook }}" name="facebook">
                                <label for="facebook">Facebook</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="twitter" value="{{ $speaker->twitter }}" name="twitter">
                                <label for="twitter">Twitter</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="google_plus" value="{{ $speaker->google_plus }}" name="google_plus">
                                <label for="google_plus">Google+</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="linkin" value="{{ $speaker->linkin }}" name="linkin">
                                <label for="linkin">Linkedin</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="topic_title" value="{{ $speaker->topic_title }}" name="topic_title">
                                <label for="topic_title">Topic Title</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                                <textarea class="form-control" rows="3" name="topic_description"
                                                          placeholder="Topic Description">{{ $speaker->topic_description }}</textarea>
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                    @if($speaker->speaker_image)
                                        <img src="{{ $speaker->speaker_image }}" alt="" />
                                    @else
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                    @endif
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="speaker_image"> </span>
                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn green">Save Changes</button>
                </div>
         {!! Form::close() !!}
