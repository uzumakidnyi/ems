<link href="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/clockface/css/clockface.css" rel="stylesheet') }}" type="text/css" />

{!! Form::open(array('url'=>'event/edit_schedule'.'/'.$event_time->event_id, 'files' => true ))  !!}
<div class="modal-body">
    <input type="hidden" name="id" value="{{ $event_time->id }}">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group form-md-line-input form-md-floating-label input-group  date date-picker"
                 data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                <input type="text" name="event_date" id="event_date"
                       value="{{ $event_time->event_date }}"
                       class="form-control">
                                                        <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>
                <label for="event_date">Event Date</label>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group form-md-line-input form-md-floating-label input-group">
                <input type="text" name="event_time"  placeholder="Event Time" value="{{ $event_time->event_time }}"
                       class="form-control timepicker timepicker-24">
                <span class="input-group-btn">
                    <button class="btn default" type="button">
                        <i class="fa fa-clock-o"></i>
                    </button>
                </span>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group form-md-line-input form-md-floating-label">
                <input type="text" class="form-control" id="event_time_title" value="{{ $event_time->event_time_title }}" name="event_time_title">
                <label for="event_time_title">Schedule Title</label>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group form-md-line-input form-md-floating-label">
                <select name="speaker_id" class="form-control" id="speaker_id">
                    <option value="" selected>Speaker List ( Optional )</option>
                    @foreach($event->speaker as $speaker)
                        <option @if($event_time->speaker_id == $speaker->id )selected @endif value="{{ $speaker->id }}">{{ $speaker->speaker_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group form-md-line-input form-md-floating-label">
                                                <textarea class="form-control" rows="3" name="event_time_description"
                                                          placeholder="Schedule Description">{{ $event_time->event_time_description }}</textarea>
                <div class="form-control-focus"></div>
            </div>
            <span class="red">if u select speaker u dont need to write Schedule Description , Speaker's topic description will use </span>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
    <button type="submit" class="btn green">Submit</button>
</div>
{!! Form::close() !!}

<script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/clockface/js/clockface.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>