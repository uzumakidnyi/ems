@extends('admin.layout.app')

@section('css')
    <link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Blank Page Layout
                        <small>blank page layout</small>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
                <!-- BEGIN PAGE TOOLBAR -->
                <div class="page-toolbar">
                    <!-- BEGIN THEME PANEL -->
                    <div class="btn-group btn-theme-panel">
                        <a href="javascript:;" class="btn dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-settings"></i>
                        </a>
                        <div class="dropdown-menu theme-panel pull-right dropdown-custom hold-on-click">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <h3>THEME</h3>
                                    <ul class="theme-colors">
                                        <li class="theme-color theme-color-default" data-theme="default">
                                            <span class="theme-color-view"></span>
                                            <span class="theme-color-name">Dark Header</span>
                                        </li>
                                        <li class="theme-color theme-color-light active" data-theme="light">
                                            <span class="theme-color-view"></span>
                                            <span class="theme-color-name">Light Header</span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-12 seperator">
                                    <h3>LAYOUT</h3>
                                    <ul class="theme-settings">
                                        <li> Layout
                                            <select class="layout-option form-control input-small input-sm">
                                                <option value="fluid" selected="selected">Fluid</option>
                                                <option value="boxed">Boxed</option>
                                            </select>
                                        </li>
                                        <li> Header
                                            <select class="page-header-option form-control input-small input-sm">
                                                <option value="fixed" selected="selected">Fixed</option>
                                                <option value="default">Default</option>
                                            </select>
                                        </li>
                                        <li> Top Dropdowns
                                            <select class="page-header-top-dropdown-style-option form-control input-small input-sm">
                                                <option value="light">Light</option>
                                                <option value="dark" selected="selected">Dark</option>
                                            </select>
                                        </li>
                                        <li> Sidebar Mode
                                            <select class="sidebar-option form-control input-small input-sm">
                                                <option value="fixed">Fixed</option>
                                                <option value="default" selected="selected">Default</option>
                                            </select>
                                        </li>
                                        <li> Sidebar Menu
                                            <select class="sidebar-menu-option form-control input-small input-sm">
                                                <option value="accordion" selected="selected">Accordion</option>
                                                <option value="hover">Hover</option>
                                            </select>
                                        </li>
                                        <li> Sidebar Position
                                            <select class="sidebar-pos-option form-control input-small input-sm">
                                                <option value="left" selected="selected">Left</option>
                                                <option value="right">Right</option>
                                            </select>
                                        </li>
                                        <li> Footer
                                            <select class="page-footer-option form-control input-small input-sm">
                                                <option value="fixed">Fixed</option>
                                                <option value="default" selected="selected">Default</option>
                                            </select>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END THEME PANEL -->
                </div>
                <!-- END PAGE TOOLBAR -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Blank Page</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span class="active">Page Layouts</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold uppercase">Basic</span>
                            </div>
                            <div class="tools"></div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%"
                                   id="sample_1">
                                <thead>
                                <tr>
                                    <th class="all">Event Tile</th>
                                    <th class="min-phone-l">Event Sub-Title</th>
                                    <th class="min-tablet">Event Status</th>
                                    <th class="none">Event Location</th>
                                    <th class="none">Event Address</th>
                                    <th class="none">Event Date</th>
                                    <th class="desktop">Complete</th>
                                    <th class="all">Feature</th>
                                    <th class="all">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($events as $event)
                                <tr>
                                    <td >{{ $event->title }}</td>
                                    <td>{{ $event->sub_title }}</td>
                                    <td >
                                        @if($event->status == 1)
                                            <span class="label label-success"> Active </span>
                                        @else
                                            <span class="label label-danger"> Not Live </span>
                                        @endif
                                    </td>
                                    <td>{{ $event->city }}</td>
                                    <td>{{ $event->address }}</td>
                                    <td>{{ date('F j, Y ', strtotime($event->event_date)) }}</td>
                                    <td>
                                        @if($event->complete == 1)
                                            <span class="label label-success"> Yes </span>
                                        @else
                                            <span class="label label-danger"> No </span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($event->feature == 1)
                                            <span class="label label-success"> Yes </span>
                                        @else
                                            <span class="label label-danger"> No </span>
                                        @endif
                                    </td>
                                    <td style="width:47%;text-align: center">
                                        <button onclick="initMap({{ $event->lat }},{{ $event->long }})" type="button" class="btn btn-sm default">View Map Detail</button>
                                        <a href="{{ url('event').'/'.$event->id }}" class="btn btn-sm blue">Add More Detail</a>
                                        @if($event->status == 1)
                                            <a href="{{ url('event/make_active').'/'.$event->id .'?type=disable'}}" class="btn btn-sm red">Make Event Disable</a>
                                        @else
                                            <a href="{{ url('event/make_active').'/'.$event->id .'?type=active'}}" class="btn btn-sm green">Make Event Live</a>
                                        @endif
                                        @if($event->feature == 1)
                                            <a href="{{ url('event/make_event_feature').'/'.$event->id .'?type=disable'}}" class="btn btn-sm red">Remove Featured</a>
                                        @else
                                            <a href="{{ url('event/make_event_feature').'/'.$event->id .'?type=feature'}}" class="btn btn-sm yellow">Make Feature</a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="portlet-body" id="map" style="display: none">
                            <a class="label label-danger" href="javascript:;" style="float: right" onclick="$('#map').hide();"> Close Map </a>
                            <div id="map-canvas"
                                 style="width:100%;height:400px;"></div>
                            <div id="ajax_msg"></div>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>

    <script>

        function initMap(mylat , mylong)
        {
            $('#map').show();
            var myLatLng = { lat: mylat, lng: mylong };
            var map = new google.maps.Map(document.getElementById('map-canvas'), {
                zoom: 14,
                center: myLatLng ,
            });

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
            });
        }

    </script>
@endsection
@section('first_page_lvl_script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfXOeoxQMK3CXttc2l-qSz1chohv9UXN4&libraries=weather,geometry,visualization,places,drawing&callback"
            async defer></script>
    <script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"
            type="text/javascript"></script>
@endsection

@section('second_page_lvl_script')
    <script src="../assets/pages/scripts/table-datatables-responsive.min.js" type="text/javascript"></script>
@endsection