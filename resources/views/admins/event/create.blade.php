@extends('admin.layout.app')
@section('content')

    <style>
        #map {
            height: 100%;
        }
        .controls {
            margin-top: 10px;
            border: 1px solid transparent;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            height: 32px;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        }

        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 300px;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }

        .pac-container {
            font-family: Roboto;
        }

        #type-selector {
            color: #fff;
            background-color: #4d90fe;
            padding: 5px 11px 0px 11px;
        }

        #type-selector label {
            font-family: Roboto;
            font-size: 13px;
            font-weight: 300;
        }
        #target {
            width: 345px;
        }

    </style>
    <div id="page_content">
        <div id="page_content_inner">

            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-1">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <h3 class="md-card-toolbar-heading-text">
                                Event Information
                            </h3>
                        </div>
                        <div class="md-card-content">
                            <form action="{{ url('event') }}" method="post" >
                                {{ csrf_field() }}
                                <div class="uk-grid uk-grid-medium form_section form_section_separator"
                                     id="d_form_section" data-uk-grid-match>
                                    <div class="uk-width-1-1">
                                        <div class="uk-grid">
                                            <div class="uk-width-1-2">
                                                <div class="parsley-row">
                                                    <label>Event Title</label>
                                                    <input type="text" class="md-input" name="title" required>
                                                </div>
                                            </div>
                                            <div class="uk-width-1-2">
                                                <div class="parsley-row">
                                                    <label>Event Sub-Title</label>
                                                    <input type="text" class="md-input" name="sub_title" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="uk-grid">
                                            <div class="uk-width-1-2">
                                                <div class="parsley-row">
                                                    <label>City</label>
                                                    <input type="text" class="md-input" name="city" required>
                                                </div>
                                            </div>
                                            <div class="uk-width-1-2">
                                                <div class="parsley-row">
                                                    <label>Event Address</label>
                                                    <input type="text" class="md-input" name="address" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="uk-grid">
                                            <div class="uk-width-1-2">
                                                <div class="parsley-row">
                                                    <label for="event_date">
                                                        Event Date
                                                <span class="req">
                                                    *
                                                </span>
                                                    </label>
                                                    <input class="md-input" data-parsley-date="" data-parsley-date-message="This value should be a valid date" data-uk-datepicker="{format:'MM.DD.YYYY'}" id="event_date" name="event_date" required="" type="text"/>
                                                </div>
                                            </div>
                                            <div class="uk-width-1-2">
                                                <div class="parsley-row">
                                                    <label>Seat</label>
                                                    <input type="text" class="md-input" name="seat" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="uk-grid">
                                            <div class="uk-width-1-1">
                                                <div class="parsley-row">
                                                    <label>Event Description</label>
                                                    <textarea name="description" class="md-input autosize_init" id="" cols="30" rows="0"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="uk-grid">
                                            <div class="uk-width-large-1-1">
                                                <div class="md-card">
                                                    <div class="md-card-toolbar">
                                                        <h3 class="md-card-toolbar-heading-text">
                                                            Markers
                                                        </h3>
                                                    </div>
                                                    <div class="md-card-content">
                                                        <div class="uk-grid" data-uk-grid-margin="">
                                                            <div class="uk-width-1-1">
                                                                <div  id="map-canvas" style="width:100%;height:400px;"></div>
                                                                <div id="ajax_msg"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="uk-grid">
                                            <div class="uk-width-1-2">
                                                <input class="md-input" placeholder="Latitude" id="input-latitude" name="lat" type="text">
                                            </div>
                                            <div class="uk-width-1-2">
                                                <input class="md-input" placeholder="Longitude" id="input-longitude" name="long" type="text">
                                            </div>
                                        </div>

                                        <input id="pac-input" class="controls" type="text" placeholder="Search Place">

                                        <div class="uk-grid">
                                            <div class="uk-width-1-1">
                                                <div class="parsley-row">
                                                    <button type="submit" class="md-btn md-btn-danger">Add Event</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>


        function initMap() {
            var mapOptions = {
                center: new google.maps.LatLng(16.798703652839684, 96.14947007373053),
                zoom: 13
            };
            var map = new google.maps.Map(document.getElementById('map-canvas'),
                    mapOptions);

            var marker_position = new google.maps.LatLng(16.798703652839684, 96.14947007373053);
            var input = /** @type {HTMLInputElement} */(
                    document.getElementById('pac-input'));

            var types = document.getElementById('type-selector');
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);

            var infowindow = new google.maps.InfoWindow();
            var marker = new google.maps.Marker({
                position : marker_position ,
                draggable: true,
                map: map,
                anchorPoint: new google.maps.Point(0, -29)
            });


            google.maps.event.addListener(marker, "mouseup", function(event) {
                $('#input-latitude').val(this.position.lat());
                $('#input-longitude').val(this.position.lng());
            });

            google.maps.event.addListener( autocomplete , 'place_changed', function() {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);
                }

                marker.setIcon(/** @type {google.maps.Icon} */({
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(35, 35)
                }));

                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                $('#input-latitude').val(place.geometry.location.lat());
                $('#input-longitude').val(place.geometry.location.lng());

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }

                infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                infowindow.open(map, marker);
            });

        }

        if ($('#map-canvas').length != 0) {
            google.maps.event.addDomListener(window, 'load', initMap);
        }

    </script>
@endsection