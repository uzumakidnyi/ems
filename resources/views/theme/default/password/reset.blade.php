@extends('theme.default.layout.main')

@section('content')


    <div id="content" xmlns="http://www.w3.org/1999/html">

        <div class="container" style="margin-top: -40px">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="owl-testi timeline-wrap">
                        <div class="testi wow flipInX" data-wow-duration="1s" data-wow-delay="400ms">
                            <form role="form" action="/password/reset" method="post">
                                {!! csrf_field() !!}

                                <input type="hidden" name="token" value="{{ $token }}">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="left-align"><i class="ion ion-ios-search"></i>&nbsp;&nbsp;E v e n t G
                                            O !&nbsp;&nbsp;
                                            <small class="right-align">Reset Password</small>
                                        </h5>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="email" name="email" value="{{ $email or old('email') }}"
                                                   class="form-control" placeholder="E-mail Adress">
                                        </div>
                                    </div>
                                    @if ($errors->has('email'))
                                        <h5 class="center-align" style="padding: 5px 0px 0px 15px;"><a
                                                    style="color: #FFF;" class="waves-attach"
                                                    href="#">{{ $errors->first('email') }}</a></h5>
                                    @endif
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="password" name="password" class="form-control"
                                                   placeholder="Password">
                                        </div>
                                    </div>
                                    @if ($errors->has('password'))
                                        <h5 class="center-align" style="padding: 5px 0px 0px 15px;"><a
                                                    style="color: #FFF;" class="waves-attach"
                                                    href="#">{{ $errors->first('password') }}</a></h5>
                                    @endif
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="password" name="password_confirmation" class="form-control"
                                                   placeholder="Re-type Password">
                                        </div>
                                    </div>
                                    @if ($errors->has('password_confirmation'))
                                        <h5 class="center-align" style="padding: 5px 0px 0px 15px;"><a
                                                    style="color: #FFF;" class="waves-attach"
                                                    href="#">{{ $errors->first('password_confirmation') }}</a></h5>
                                    @endif
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <button type="submit" style="width: 140px" class="btn">Reset Password!</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        @include('theme.default.layout.footer')

    </div>
@endsection