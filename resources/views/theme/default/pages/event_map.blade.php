@extends('theme.default.layout.main')

@section('content')
    <div id="content" xmlns="http://www.w3.org/1999/html">
        <div class="container" style="margin-top: 50px;">
            <div class="row">
                <div id="map" style="height: 550px"></div>
            </div>
        </div>

        <div class="clearfix"></div>

        @include('theme.default.layout.footer')

    </div>
@endsection

@section('script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfXOeoxQMK3CXttc2l-qSz1chohv9UXN4&libraries=weather,geometry,visualization,places,drawing&callback=initMap"
            async defer></script>

    <script>

        function initMap() {

            $.ajax({
                type: 'get',
                url: '{{ url('aa')}}',
                data: '',
                dataType: 'json',
                success: function (data) {

                    var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 14,
                        center: {lat: 16.798703652839684, lng: 96.14947007373053}
                    });

                    $.each(data, function (key, val) {

                        var marker = new google.maps.Marker({
                            position: {lat: val.lat, lng: val.long},
                            map: map,
                            title: val.title
                        });

//                        var contentString = '<div class="owl-testi"><div class="testi">'+val.description+'</div></div>';

                        var contentString = '<div id="content">' +
                                                '<div id="siteNotice">' +
                                                '</div>' +
                                                '<h2 id="firstHeading" class="firstHeading">'+val.title+'</h2>' +
                                '               <div id="bodyContent">' +
                                                    '<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large ' +
                                                        val.description +
                                                    '<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">' +
                                                    'https://en.wikipedia.org/w/index.php?title=Uluru</a> ' +
                                                    '(last visited June 22, 2009).</p>' +
                                                '</div>' +
                                        '</div>';


                        var infowindow = new google.maps.InfoWindow({
                            content: contentString
                        });

                        marker.addListener('click', function () {
                            infowindow.open(map, marker);
                        });


                    });

                },
                error: function (data) {
                    swal({
                        type: 'error',
                        title: 'Oops !',
                        html: 'Something Went Wrong!',
                        showConfirmButton: false
                    });
                }
            });


        }


        if ($('#map').length != 0) {
            google.maps.event.addDomListener(window, 'load', initMap);
        }
    </script>
@endsection