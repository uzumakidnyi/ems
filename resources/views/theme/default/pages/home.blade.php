@extends('theme.default.layout.main')

@section('content')

    <div id="banner">
        <div class="tp-banner-container">
            <div class="tp-banner" >
                <ul>
                    @foreach($event as $event_list)
                    <!-- SLIDE  -->
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="images/thumb-1.jpg"  data-saveperformance="off"  data-title="Slide">
                        <!-- MAIN IMAGE -->
                        <img src="{{ url('theme/default/images/bg_slider_1.jpg') }}"  alt="fullslide1"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

                        <!-- LAYERS -->
                        <div class="tp-caption light_medium_20 tp-fade fadeout tp-resizeme"
                             data-x="left" data-hoffset="40"
                             data-y="center" data-voffset="-60"
                             data-speed="500"
                             data-start="500"
                             data-easing="Power4.easeOut"
                             data-splitin="chars"
                             data-splitout="chars"
                             data-elementdelay="0.05"
                             data-endelementdelay="0.05"
                             data-endspeed="300"
                             style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">NEW - {{ date('F' , strtotime($event_list->event_date)) }} </div>

                        <!-- LAYERS -->
                        <div class="tp-caption white_heavy_70 tp-fade fadeout tp-resizeme"
                             data-x="left" data-hoffset="40"
                             data-y="center" data-voffset="0"
                             data-speed="500"
                             data-start="500"
                             data-easing="Power4.easeOut"
                             data-splitin="chars"
                             data-splitout="chars"
                             data-elementdelay="0.05"
                             data-endelementdelay="0.05"
                             data-endspeed="300"
                             style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">{{ $event_list->title }}</div>

                        <!-- LAYERS -->
                        <div class="tp-caption white_heavy_48_light tp-fade fadeout tp-resizeme"
                             data-x="left" data-hoffset="40"
                             data-y="center" data-voffset="25"
                             data-speed="700"
                             data-start="700"
                             data-easing="Power4.easeOut"
                             data-splitin="chars"
                             data-splitout="chars"
                             data-elementdelay="0.05"
                             data-endelementdelay="0.05"
                             data-endspeed="300"
                             style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;"> {{ $event_list->sub_title }}...</div>

                        <!-- LAYERS -->
                        <div class="tp-caption black_thin_30 tp-fade fadeout tp-resizeme"
                             data-x="left" data-hoffset="40"
                             data-y="center" data-voffset="100"
                             data-speed="700"
                             data-start="700"
                             data-easing="Power4.easeOut"
                             data-splitin="chars"
                             data-splitout="chars"
                             data-elementdelay="0.05"
                             data-endelementdelay="0.05"
                             data-endspeed="300"
                             style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;"> <i class="ion-ios7-clock"></i>Event Date
                            : {{ date('F j, Y' , strtotime($event_list->event_date)) }}

                            @if($event_list->event_time()->orderBy('event_date','desc')->first()->event_date != $event_list->event_date)
                                -
                                {{ date('F j, Y' , strtotime($event_list->event_time()->orderBy('event_date','desc')->first()->event_date )) }}
                            @endif</div>

                        <!-- LAYERS -->
                        <div class="tp-caption customin tp-resizeme rs-parallaxlevel-0"
                             data-x="left" data-hoffset="40"
                             data-y="center" data-voffset="200"
                             data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-speed="1000"
                             data-start="1000"
                             data-easing="Power3.easeInOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.1"
                             data-endelementdelay="0.1"
                             data-linktoslide="next"
                             style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;"><a href='#' class='largeredbtn'>REGISTER</a> </div>

                        <!-- LAYERS -->

                        <?php $title = \Illuminate\Support\Str::slug($event_list->title) ?>
                        <div class="tp-caption customin tp-resizeme rs-parallaxlevel-0"
                             data-x="left" data-hoffset="280"
                             data-y="center" data-voffset="200"
                             data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-speed="1200"
                             data-start="1200"
                             data-easing="Power3.easeInOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.1"
                             data-endelementdelay="0.1"
                             data-linktoslide="next"
                             style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;"><a href='{{ url('event-detail').'/'.$title.'?id='.$event_list->id }}' class='largeredbtn'>LEARN MORE</a> </div>
                    </li>
                    <!-- SLIDE  -->
                    @endforeach
                </ul>
                <div class="tp-bannertimer"></div>
            </div>
        </div>
    </div>

    <div id="content">

        <!--======= Menage Singer =========-->
        <div class="manage">
            <div class="container">
                <ul class="row">

                    <!--======= France =========-->
                    <li class="col-md-3 wow rotateInUpLeft" data-wow-duration="1s" data-wow-delay="200ms">
                        <div class="icon"><i class="ion-ios7-location-outline"></i></div>
                        <h2>France</h2>
                        <p>Resident Hotel, 1000</p>
                    </li>

                    <!--======= Speakers =========-->
                    <li class="col-md-3 wow rotateInUpLeft" data-wow-duration="1s" data-wow-delay="400ms">
                        <div class="icon"><i class="ion-ios7-mic-outline"></i></div>
                        <h2>35 Speakers</h2>
                        <p>Best specialists.</p>
                    </li>

                    <!--======= Sponsor =========-->
                    <li class="col-md-3 wow rotateInUpLeft" data-wow-duration="1s" data-wow-delay="600ms">
                        <div class="icon"><i class="ion-ios7-people-outline"></i></div>
                        <h2>54 Sponsor</h2>
                        <p>Silver, Gold and Platin</p>
                    </li>

                    <!--======= Seats  =========-->
                    <li class="col-md-3 wow rotateInUpLeft" data-wow-duration="1s" data-wow-delay="800ms">
                        <div class="icon"><i class="ion-ios7-person-outline"></i></div>
                        <h2>1458 Seats</h2>
                        <p>Hurry up, register!</p>
                    </li>
                </ul>
            </div>
        </div>

        <!--======= Event Days =========-->
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="days wow bounceIn" data-wow-duration="1s" data-wow-delay="300ms">

                        <!-- Nav tabs -->
                        <nav>
                            <ul class="nav nav-tabs nav-pills" role="tablist">
                                <li class="active"><a href="#Day-1" role="tab" data-toggle="tab">Day 1</a></li>
                                <li><a href="#Day-2" role="tab" data-toggle="tab">Day 2</a></li>
                                <li><a href="#Day-3" role="tab" data-toggle="tab">Day 3</a></li>
                            </ul>
                        </nav>
                        <!-- Tab panes -->
                        <div class="tab-content">

                            <!--==========Days 1 Start==========-->
                            <div class="tab-pane fade in active" id="Day-1">
                                <p>It is a long established fact that a reader will be distracted by the readable content.</p>
                                <div class="panel-group" id="accordion">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">

                                            <!--=======  Panel Details =========-->
                                            <div class="panel-title">
                                                <div class="e-time"><i class="ion-ios7-clock"></i> 09:30</div>
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                    <p>Lorem Ipsum is simply dummy text.</p>
                                                </a></div>
                                        </div>

                                        <!--=======  collapseOne =========-->
                                        <div id="collapseOne" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                    Lorem Ipsum has been the industry's standard dummy text ever since the
                                                    1500s, when an unknown printer took a galley of type and scrambled it to
                                                    make a type specimen book. <br>
                                                    <br>
                                                    It has survived not only five centuries, but also the leap into electronic
                                                    typesetting, remaining essentially unchanged. </p>
                                            </div>
                                        </div>
                                    </div>

                                    <!--=======  Panel Details =========-->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <div class="e-time"><i class="ion-ios7-clock"></i> 11:20</div>
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"
                                                   class="collapsed">
                                                    <p>Lorem Ipsum is simply dummy text.</p>
                                                </a></div>
                                        </div>

                                        <!--=======  collapseTwo =========-->
                                        <div id="collapseTwo" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                    Lorem Ipsum has been the industry's standard dummy text ever since the
                                                    1500s, when an unknown printer took a galley of type and scrambled it to
                                                    make a type specimen book. <br>
                                                    <br>
                                                    It has survived not only five centuries, but also the leap into electronic
                                                    typesetting, remaining essentially unchanged. </p>
                                            </div>
                                        </div>
                                    </div>

                                    <!--=======  Panel Details =========-->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <div class="e-time"><i class="ion-ios7-clock"></i> 13:00</div>
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"
                                                   class="collapsed">
                                                    <p>Lorem Ipsum is simply dummy text.</p>
                                                </a></div>
                                        </div>

                                        <!--=======  collapseThree =========-->
                                        <div id="collapseThree" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                    Lorem Ipsum has been the industry's standard dummy text ever since the
                                                    1500s, when an unknown printer took a galley of type and scrambled it to
                                                    make a type specimen book. <br>
                                                    <br>
                                                    It has survived not only five centuries, but also the leap into electronic
                                                    typesetting, remaining essentially unchanged. </p>
                                            </div>
                                        </div>
                                    </div>

                                    <!--=======  Panel Details =========-->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <div class="e-time"><i class="ion-ios7-clock"></i> 14:20</div>
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour"
                                                   class="collapsed">
                                                    <p>Lorem Ipsum is simply dummy text.</p>
                                                </a></div>
                                        </div>

                                        <!--=======  collapsefour =========-->
                                        <div id="collapsefour" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                    Lorem Ipsum has been the industry's standard dummy text ever since the
                                                    1500s, when an unknown printer took a galley of type and scrambled it to
                                                    make a type specimen book. <br>
                                                    <br>
                                                    It has survived not only five centuries, but also the leap into electronic
                                                    typesetting, remaining essentially unchanged. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--==========Days 2 Start==========-->
                            <div class="tab-pane fade" id="Day-2">
                                <p>It is a long established fact that a reader will be distracted by the readable content.</p>
                                <div class="panel-group" id="accordion1">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <div class="e-time"><i class="ion-ios7-clock"></i> 10:30</div>
                                                <a data-toggle="collapse" data-parent="#accordion1" href="#collapseFive">
                                                    <p>Lorem Ipsum is simply dummy text.</p>
                                                </a></div>
                                        </div>

                                        <!--=======  collapseFive =========-->
                                        <div id="collapseFive" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                    Lorem Ipsum has been the industry's standard dummy text ever since the
                                                    1500s, when an unknown printer took a galley of type and scrambled it to
                                                    make a type specimen book. <br>
                                                    <br>
                                                    It has survived not only five centuries, but also the leap into electronic
                                                    typesetting, remaining essentially unchanged. </p>
                                            </div>
                                        </div>
                                    </div>

                                    <!--=======  Panel Details =========-->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <div class="e-time"><i class="ion-ios7-clock"></i> 12:20</div>
                                                <a data-toggle="collapse" data-parent="#accordion1" href="#collapseSix"
                                                   class="collapsed">
                                                    <p>Lorem Ipsum is simply dummy text.</p>
                                                </a></div>
                                        </div>

                                        <!--=======  collapseSix =========-->
                                        <div id="collapseSix" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                    Lorem Ipsum has been the industry's standard dummy text ever since the
                                                    1500s, when an unknown printer took a galley of type and scrambled it to
                                                    make a type specimen book. <br>
                                                    <br>
                                                    It has survived not only five centuries, but also the leap into electronic
                                                    typesetting, remaining essentially unchanged. </p>
                                            </div>
                                        </div>
                                    </div>

                                    <!--=======  Panel Details =========-->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <div class="e-time"><i class="ion-ios7-clock"></i> 14:00</div>
                                                <a data-toggle="collapse" data-parent="#accordion1" href="#collapseSeven"
                                                   class="collapsed">
                                                    <p>Lorem Ipsum is simply dummy text.</p>
                                                </a></div>
                                        </div>

                                        <!--=======  collapseSeven =========-->
                                        <div id="collapseSeven" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                    Lorem Ipsum has been the industry's standard dummy text ever since the
                                                    1500s, when an unknown printer took a galley of type and scrambled it to
                                                    make a type specimen book. <br>
                                                    <br>
                                                    It has survived not only five centuries, but also the leap into electronic
                                                    typesetting, remaining essentially unchanged. </p>
                                            </div>
                                        </div>
                                    </div>

                                    <!--=======  Panel Details =========-->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <div class="e-time"><i class="ion-ios7-clock"></i> 15:20</div>
                                                <a data-toggle="collapse" data-parent="#accordion1" href="#collapseEight"
                                                   class="collapsed">
                                                    <p>Lorem Ipsum is simply dummy text.</p>
                                                </a></div>
                                        </div>

                                        <!--=======  collapseEight =========-->
                                        <div id="collapseEight" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                    Lorem Ipsum has been the industry's standard dummy text ever since the
                                                    1500s, when an unknown printer took a galley of type and scrambled it to
                                                    make a type specimen book. <br>
                                                    <br>
                                                    It has survived not only five centuries, but also the leap into electronic
                                                    typesetting, remaining essentially unchanged. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--==========Days 3 Start==========-->
                            <div class="tab-pane fade" id="Day-3">
                                <p>It is a long established fact that a reader will be distracted by the readable content.</p>
                                <div class="panel-group" id="accordion2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <div class="e-time"><i class="ion-ios7-clock"></i> 16:30</div>
                                                <a data-toggle="collapse" data-parent="#accordion2" href="#collapseNine">
                                                    <p>Lorem Ipsum is simply dummy text.</p>
                                                </a></div>
                                        </div>

                                        <!--=======  collapseNine =========-->
                                        <div id="collapseNine" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                    Lorem Ipsum has been the industry's standard dummy text ever since the
                                                    1500s, when an unknown printer took a galley of type and scrambled it to
                                                    make a type specimen book. <br>
                                                    <br>
                                                    It has survived not only five centuries, but also the leap into electronic
                                                    typesetting, remaining essentially unchanged. </p>
                                            </div>
                                        </div>
                                    </div>

                                    <!--=======  Panel Details =========-->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <div class="e-time"><i class="ion-ios7-clock"></i> 17:20</div>
                                                <a data-toggle="collapse" data-parent="#accordion2" href="#collapseTen"
                                                   class="collapsed">
                                                    <p>Lorem Ipsum is simply dummy text.</p>
                                                </a></div>
                                        </div>

                                        <!--=======  collapseTen =========-->
                                        <div id="collapseTen" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                    Lorem Ipsum has been the industry's standard dummy text ever since the
                                                    1500s, when an unknown printer took a galley of type and scrambled it to
                                                    make a type specimen book. <br>
                                                    <br>
                                                    It has survived not only five centuries, but also the leap into electronic
                                                    typesetting, remaining essentially unchanged. </p>
                                            </div>
                                        </div>
                                    </div>

                                    <!--=======  Panel Details =========-->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <div class="e-time"><i class="ion-ios7-clock"></i> 18:00</div>
                                                <a data-toggle="collapse" data-parent="#accordion2" href="#collapseEleven"
                                                   class="collapsed">
                                                    <p>Lorem Ipsum is simply dummy text.</p>
                                                </a></div>
                                        </div>

                                        <!--=======  collapseEleven =========-->
                                        <div id="collapseEleven" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                    Lorem Ipsum has been the industry's standard dummy text ever since the
                                                    1500s, when an unknown printer took a galley of type and scrambled it to
                                                    make a type specimen book. <br>
                                                    <br>
                                                    It has survived not only five centuries, but also the leap into electronic
                                                    typesetting, remaining essentially unchanged. </p>
                                            </div>
                                        </div>
                                    </div>

                                    <!--=======  Panel Details =========-->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <div class="e-time"><i class="ion-ios7-clock"></i> 19:20</div>
                                                <a data-toggle="collapse" data-parent="#accordion2" href="#collapseTwelve"
                                                   class="collapsed">
                                                    <p>Lorem Ipsum is simply dummy text.</p>
                                                </a></div>
                                        </div>

                                        <!--=======  collapseTwelve =========-->
                                        <div id="collapseTwelve" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                    Lorem Ipsum has been the industry's standard dummy text ever since the
                                                    1500s, when an unknown printer took a galley of type and scrambled it to
                                                    make a type specimen book. <br>
                                                    <br>
                                                    It has survived not only five centuries, but also the leap into electronic
                                                    typesetting, remaining essentially unchanged. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--======= Event Testimonals =========-->
                <div class="col-md-5">
                    <div id="owl-testi">

                        <!--======= Testimonals 1=========-->
                        <div class="testi">
                            <h5>Event Testimonals</h5>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                                the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                                of type and scrambled it to make a type specimen book. </p>
                            <p> It has survived not only five centuries, but also the leap into electronic typesetting,
                                remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                                sheets containing. </p>
                            <p> Lorem Ipsum passages, and more recently.</p>
                            <div class="author-name">
                                <h3><em>Quen Gourde</em></h3>
                                <p>Alluguitar Music Store , Ceo</p>
                            </div>
                            <div class="author-pic"><img src="theme/default/images/testi-img-1.png" alt=""></div>
                        </div>

                        <!--======= Testimonals 2 =========-->
                        <div class="testi">
                            <h5>Event Testimonals</h5>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                                the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                                of type and scrambled it to make a type specimen book. </p>
                            <p> It has survived not only five centuries, but also the leap into electronic typesetting,
                                remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                                sheets containing. </p>
                            <p> Lorem Ipsum passages, and more recently.</p>
                            <div class="author-name">
                                <h3><em>Quen Gourde</em></h3>
                                <p>Alluguitar Music Store , Ceo</p>
                            </div>
                            <div class="author-pic"><img src="theme/default/images/testi-img-2.png" alt=""></div>
                        </div>

                        <!--======= Testimonals 3 =========-->
                        <div class="testi">
                            <h5>Event Testimonals</h5>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                                the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                                of type and scrambled it to make a type specimen book. </p>
                            <p> It has survived not only five centuries, but also the leap into electronic typesetting,
                                remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                                sheets containing. </p>
                            <p> Lorem Ipsum passages, and more recently.</p>
                            <div class="author-name">
                                <h3><em>Quen Gourde</em></h3>
                                <p>Alluguitar Music Store , Ceo</p>
                            </div>
                            <div class="author-pic"><img src="theme/default/images/testi-img-3.png" alt=""></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <!--======= Gallery Section =========-->
        <div id="gallery">
            <div class="container">

                <div class="row">

                    <!--======= Large Image =========-->
                    <ul class="col-md-6 posts">
                        <li><span class="img"> <img src="theme/default/images/img_1.jpg" alt=""></span>
                            <div class="over">
                                <div class="icon"><i class="ion-ios7-photos-outline"></i></div>

                                <!--=======  Heading Tittle =========-->
                                <div class="tittle">
                                    <h2>EventGO! <span>Startup Photo Gallery</span></h2>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                        has been the industry's standard dummy text ever since the 1500s, when an.Lorem ipsum
                                        dolor sit amet, consectetur adipiscing elit. Suspendisse est odioconvallis. </p>
                                </div>
                            </div>
                        </li>
                    </ul>

                    <!--======= Large Image =========-->
                    <ul class="col-md-6 posts">
                        <li><span class="img"> <img src="theme/default/images/img_2.jpg" alt=""></span>
                            <div class="over">
                                <div class="icon"><i class="ion-ios7-photos-outline"></i></div>

                                <!--=======  Heading Tittle =========-->
                                <div class="tittle">
                                    <h2>EventGO! <span>Startup Photo Gallery</span></h2>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                        has been the industry's standard dummy text ever since the 1500s, when an.Lorem ipsum
                                        dolor sit amet, consectetur adipiscing elit. Suspendisse est odioconvallis. </p>
                                </div>
                            </div>
                        </li>
                    </ul>

                    <!--======= Small Images =========-->
                    <div class="col-md-6">
                        <ul class="row gallery-thump posts">
                            <li class="col-xs-4">
                                <div class="inner-thump"><span class="img"> <img src="theme/default/images/img_3.jpg"
                                                                                 alt=""></span>
                                    <div class="over"><a href="images/img_3.jpg" data-rel="prettyPhoto"
                                                         class="prettyPhoto lightzoom zoom"><i
                                                    class="ion-ios7-search-strong"></i></a></div>
                                </div>
                            </li>
                            <li class="col-xs-4">
                                <div class="inner-thump"><span class="img"> <img src="theme/default/images/img_5.jpg"
                                                                                 alt=""></span>
                                    <div class="over"><a href="images/img_3.jpg" data-rel="prettyPhoto"
                                                         class="prettyPhoto lightzoom zoom"><i
                                                    class="ion-ios7-search-strong"></i></a></div>
                                </div>
                            </li>
                            <li class="col-xs-4">
                                <div class="inner-thump"><span class="img"> <img src="theme/default/images/img_6.jpg"
                                                                                 alt=""></span>
                                    <div class="over"><a href="images/img_3.jpg" data-rel="prettyPhoto"
                                                         class="prettyPhoto lightzoom zoom"><i
                                                    class="ion-ios7-search-strong"></i></a></div>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <!--======= Small Images =========-->
                    <div class="col-md-6">
                        <ul class="row gallery-thump posts">
                            <li class="col-xs-4">
                                <div class="inner-thump"><span class="img"> <img src="theme/default/images/img_3.jpg"
                                                                                 alt=""></span>
                                    <div class="over"><a href="images/img_3.jpg" data-rel="prettyPhoto"
                                                         class="prettyPhoto lightzoom zoom"><i
                                                    class="ion-ios7-search-strong"></i></a></div>
                                </div>
                            </li>
                            <li class="col-xs-4">
                                <div class="inner-thump"><span class="img"> <img src="theme/default/images/img_5.jpg"
                                                                                 alt=""></span>
                                    <div class="over"><a href="images/img_3.jpg" data-rel="prettyPhoto"
                                                         class="prettyPhoto lightzoom zoom"><i
                                                    class="ion-ios7-search-strong"></i></a></div>
                                </div>
                            </li>
                            <li class="col-xs-4">
                                <div class="inner-thump"><span class="img"> <img src="theme/default/images/img_6.jpg"
                                                                                 alt=""></span>
                                    <div class="over"><a href="images/img_3.jpg" data-rel="prettyPhoto"
                                                         class="prettyPhoto lightzoom zoom"><i
                                                    class="ion-ios7-search-strong"></i></a></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!--======= Newsletter =========-->
        <div id="news">
            <div class="container">
                <div class="icon-main wow flipInX" data-wow-duration="1s" data-wow-delay="600ms"><i
                            class="ion-ios7-email-outline"></i></div>

                <!--=======  Heading Tittle =========-->
                <div class="tittle wow flipInX" data-wow-duration="1s" data-wow-delay="800ms">
                    <h2>EventGO! <span>Startup Newsletter</span></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit eaque dolorem fugiat ullam ea!
                        Perspiciatis, ipsum ea possimus voluptatem sequi
                        amet harum dolores commodi eveniet earum est expedita error ab.</p>
                </div>
                <div class="input-news">
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="" value="E-mail address ..">
                        <input name="submit" type="submit" value="SUBSCRIBE"/>
                    </div>
                </div>
            </div>
        </div>

        <!--======= Counter Timer =========-->
        <div id="timer">
            <div class="container">
                <div class="time">

                    <!-- Countdown-->
                    <ul class="row countdown">

                        <!--======= Days =========-->
                        <li class="col-sm-3"><span class="days">00</span>
                            <p class="days_ref">days</p>
                        </li>

                        <!--======= Houre =========-->
                        <li class="col-sm-3 ">
                            <div class="round-back"><span class="hours">00</span>
                                <p class="hours_ref">hours</p>
                            </div>
                        </li>

                        <!--======= Mintes =========-->
                        <li class="col-sm-3"><span class="minutes">00</span>
                            <p class="minutes_ref">minutes</p>
                        </li>

                        <!--======= Seconds =========-->
                        <li class="col-sm-3"><span class="seconds">00</span>
                            <p class="seconds_ref">seconds</p>
                        </li>
                    </ul>
                    <!-- Countdown end-->
                </div>
            </div>
        </div>

        <!--=======  Member Startup Speakers =========-->
        <div id="member">
            <div class="container">
                <div class="icon-main wow flipInX" data-wow-duration="1s" data-wow-delay="600ms"><i
                            class="ion-ios7-people-outline"></i></div>

                <!--=======  Heading Tittle =========-->
                <div class="tittle wow flipInX" data-wow-duration="1s" data-wow-delay="800ms">
                    <h2>EventGO! <span>Startup Speakers</span></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit eaque dolorem fugiat ullam ea!
                        Perspiciatis, ipsum ea possimus voluptatem sequi
                        amet harum dolores commodi eveniet earum est expedita error ab.</p>
                </div>

                <!--=======  Team Members =========-->
                <div class="team">
                    <ul class="row grid">

                        <!--=======  Member 1 =========-->
                        <li class="col-md-4">
                            <div class="figure effect-oscar"><img src="theme/default/images/team-1.png" alt=""/>
                                <div class="figcaption">
                                    <p><a href="#." class="fb"> <span class="ion-social-facebook"></span></a> <a href="#."
                                                                                                                 class="tw">
                                            <span class="ion-social-twitter"></span></a> <a href="#." class="linkedin"> <span
                                                    class="ion-social-linkedin-outline"></span> </a></p>
                                </div>

                                <!--=======  Member Name Details =========-->
                                <div class="mem-detail">
                                    <div class="icon"><i class="ion-ios7-mic-outline"></i></div>
                                    <h4>Alex <span>GOUNDE</span></h4>
                                    <p>Noturdamus Corporate - CEO</p>
                                </div>
                            </div>
                        </li>

                        <!--=======  Member 2 =========-->
                        <li class="col-md-4">
                            <div class="figure effect-oscar"><img src="theme/default/images/team-2.png" alt=""/>
                                <div class="figcaption">
                                    <p><a href="#." class="fb"> <span class="ion-social-facebook"></span></a> <a href="#."
                                                                                                                 class="tw">
                                            <span class="ion-social-twitter"></span></a> <a href="#." class="linkedin"> <span
                                                    class="ion-social-linkedin-outline"></span> </a></p>
                                </div>

                                <!--=======  Member Name Details =========-->
                                <div class="mem-detail">
                                    <div class="icon"><i class="ion-ios7-mic-outline"></i></div>
                                    <h4>Alex <span>GOUNDE</span></h4>
                                    <p>Noturdamus Corporate - CEO</p>
                                </div>
                            </div>
                        </li>

                        <!--=======  Member 3 =========-->
                        <li class="col-md-4">
                            <div class="figure effect-oscar"><img src="theme/default/images/team-3.png" alt=""/>
                                <div class="figcaption">
                                    <p><a href="#." class="fb"> <span class="ion-social-facebook"></span></a> <a href="#."
                                                                                                                 class="tw">
                                            <span class="ion-social-twitter"></span></a> <a href="#." class="linkedin"> <span
                                                    class="ion-social-linkedin-outline"></span> </a></p>
                                </div>

                                <!--=======  Member Name Details =========-->
                                <div class="mem-detail">
                                    <div class="icon"><i class="ion-ios7-mic-outline"></i></div>
                                    <h4>Alex <span>GOUNDE</span></h4>
                                    <p>Noturdamus Corporate - CEO</p>
                                </div>
                            </div>
                        </li>

                        <!--=======  Member 4 =========-->
                        <li class="col-md-4">
                            <div class="figure effect-oscar"><img src="theme/default/images/team-4.png" alt=""/>
                                <div class="figcaption">
                                    <p><a href="#." class="fb"> <span class="ion-social-facebook"></span></a> <a href="#."
                                                                                                                 class="tw">
                                            <span class="ion-social-twitter"></span></a> <a href="#." class="linkedin"> <span
                                                    class="ion-social-linkedin-outline"></span> </a></p>
                                </div>

                                <!--=======  Member Name Details =========-->
                                <div class="mem-detail">
                                    <div class="icon"><i class="ion-ios7-mic-outline"></i></div>
                                    <h4>Alex <span>GOUNDE</span></h4>
                                    <p>Noturdamus Corporate - CEO</p>
                                </div>
                            </div>
                        </li>

                        <!--=======  Member 5 =========-->
                        <li class="col-md-4">
                            <div class="figure effect-oscar"><img src="theme/default/images/team-5.png" alt=""/>
                                <div class="figcaption">
                                    <p><a href="#." class="fb"> <span class="ion-social-facebook"></span></a> <a href="#."
                                                                                                                 class="tw">
                                            <span class="ion-social-twitter"></span></a> <a href="#." class="linkedin"> <span
                                                    class="ion-social-linkedin-outline"></span> </a></p>
                                </div>

                                <!--=======  Member Name Details =========-->
                                <div class="mem-detail">
                                    <div class="icon"><i class="ion-ios7-mic-outline"></i></div>
                                    <h4>Alex <span>GOUNDE</span></h4>
                                    <p>Noturdamus Corporate - CEO</p>
                                </div>
                            </div>
                        </li>

                        <!--=======  Member 6 =========-->
                        <li class="col-md-4">
                            <div class="figure effect-oscar"><img src="theme/default/images/team-6.png" alt=""/>
                                <div class="figcaption">
                                    <p><a href="#." class="fb"> <span class="ion-social-facebook"></span></a> <a href="#."
                                                                                                                 class="tw">
                                            <span class="ion-social-twitter"></span></a> <a href="#." class="linkedin"> <span
                                                    class="ion-social-linkedin-outline"></span> </a></p>
                                </div>

                                <!--=======  Member Name Details =========-->
                                <div class="mem-detail">
                                    <div class="icon"><i class="ion-ios7-mic-outline"></i></div>
                                    <h4>Alex <span>GOUNDE</span></h4>
                                    <p>Noturdamus Corporate - CEO</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <!--=======  End =========-->
                </div>
            </div>
        </div>

        <!-- Start Google Map -->
        <div id="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3152.0424143658906!2d144.953892!3d-37.81247549999982!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad65d48de42c403%3A0xf7e95c9095fac225!2sKing+St%2C+Melbourne+VIC%2C+Australia!5e0!3m2!1sen!2s!4v1405448972057"></iframe>
        </div>
        <!-- End Google Map -->

        @include('theme.default.layout.footer')

    </div>

@endsection