@extends('theme.default.layout.main')

@section('content')

    <style>
        .flex-container {
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-flex-wrap: wrap;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-align-content: flex-start;
            -ms-flex-line-pack: start;
            align-content: flex-start;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;
        }

        .flex-item {
            -webkit-order: 0;
            -ms-flex-order: 0;
            order: 0;
            -webkit-flex: 0 1 auto;
            -ms-flex: 0 1 auto;
            flex: 0 1 auto;
            -webkit-align-self: auto;
            -ms-flex-item-align: auto;
            align-self: auto;
        }

    </style>

    @include('theme.default.layout.banner')

    <div id="content">

        <!--======= Event Time =========-->
        <div id="event_time">
            <div class="container">
                <ul>

                    <li>
                        <span class="top-25">{{ date('j F',strtotime($event->event_date)) }}</span> <br>
                        <span class="big">{{ date('Y',strtotime($event->event_date)) }}</span>
                    </li>

                    <!--======= Event Time =========-->
                    <li class="icon"><i class="ion-ios7-clock-outline"></i></li>

                    <!--======= Event Time =========-->
                    <li>
                        <span class="top-25">Events Time</span><br>
                        <span class="big">{{ date('G:i', strtotime($event->event_time()->orderBy('event_time','asc')->first()->event_time)) }}</span>
                    </li>

                </ul>
            </div>
        </div>

        <!--======= Menage Singer =========-->
        <div class="manage">
            <div class="container">
                <ul class="row">

                    <!--======= France =========-->
                    <li class="col-md-3 wow rotateInUpLeft" data-wow-duration="1s" data-wow-delay="200ms">
                        <div class="icon"><i class="ion-ios7-location-outline"></i></div>
                        <h2>{{ $event->city }}</h2>
                        <p>{{ $event->address }}</p>
                    </li>

                    <!--======= Speakers =========-->
                    @if($event->speaker)
                        <li class="col-md-3 wow rotateInUpLeft" data-wow-duration="1s" data-wow-delay="400ms">
                            <div class="icon"><i class="ion-ios7-mic-outline"></i></div>
                            <h2>{{ $event->speaker()->count() }} Speakers</h2>
                            <p>Best specialists.</p>
                        </li>
                        @endif
                                <!--======= Sponsor =========-->
                        <li class="col-md-3 wow rotateInUpLeft" data-wow-duration="1s" data-wow-delay="600ms">
                            <div class="icon"><i class="ion-ios7-people-outline"></i></div>
                            <h2>{{ $event->event_sponsor()->count() }} Sponsor</h2>
                            <p>Silver, Gold and Platin</p>
                        </li>

                        <!--======= Seats  =========-->
                        <li class="col-md-3 wow rotateInUpLeft" data-wow-duration="1s" data-wow-delay="800ms">
                            <div class="icon"><i class="ion-ios7-person-outline"></i></div>
                            <h2>{{ $event->seat }} Seats</h2>
                            <p>Hurry up, register!</p>
                        </li>
                </ul>
            </div>
        </div>

        <!--======= Event Days =========-->
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="days wow bounceIn" data-wow-duration="1s" data-wow-delay="300ms">

                        <!-- Nav tabs -->
                        <nav>
                            <ul class="nav nav-tabs nav-pills" role="tablist">
                                <?php $i = 1 ?>
                                @foreach($event->event_time()->groupBy('event_date')->get() as $key => $date)
                                    <li @if($key == 0 ) class="active" @endif >
                                        <a href="#Day-{{ $i }}" role="tab" data-toggle="tab">Day {{ $i }} </a>
                                    </li>
                                    <?php $i++ ?>
                                @endforeach
                            </ul>
                        </nav>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <?php $i = 1; $x = 1 ?>
                            @foreach($event->event_time()->groupBy('event_date')->get() as $key => $date)
                                <div class="tab-pane fade @if($key == 0 ) in active @endif" id="Day-{{ $i }}">
                                    <p>
                                        <i class="ion-ios7-calendar"></i>&nbsp;&nbsp;{{ date('F j, Y - l' , strtotime($date->event_date)) }}
                                    </p>

                                    @foreach($event->event_time()->where('event_date',$date->event_date)->orderBy('event_time','asc')->get() as $key => $data )
                                        <div class="panel-group" id="accordion{{ $x }}">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <div class="panel-title">
                                                        <div class="e-time"><i
                                                                    class="ion-ios7-clock"></i> {{ date('G:i' , strtotime($data->event_time)) }}
                                                        </div>
                                                        <a data-toggle="collapse" data-parent="#accordion{{ $x }}"
                                                           href="#collapse{{ $x }}"
                                                           @if($key != 0 ) class="collapsed" @endif>
                                                            <p style="min-height:250px"> {{ $data->event_time_title }}</p>
                                                        </a></div>
                                                </div>
                                                <div id="collapse{{ $x }}"
                                                     class="panel-collapse collapse @if($key == 0 ) in @endif">
                                                    <div class="panel-body">
                                                        @if($data->speaker_id)
                                                            <h3 style="color: #000;font-weight: 500">{{ $event->speaker->find($data->speaker_id)->topic_title }}</h3>
                                                            <br>
                                                            <p>{{ $event->speaker->find($data->speaker_id)->topic_description }}</p>
                                                        @else
                                                            <p>{{ $data->event_time_description }}</p>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php $x++ ?>
                                    @endforeach
                                </div>
                                <?php $i++ ?>
                            @endforeach
                        </div>
                    </div>
                </div>

                @if($event->event_testimonal()->count() > 0)
                        <!--======= Event Testimonals =========-->
                <div class="col-md-5">
                    <div id="owl-testi">
                        @foreach($event->event_testimonal as $test )
                            <div class="testi">
                                <h5>Event Testimonals</h5>
                                <p style="min-height: 250px">{{ $test->testimonal_description }}</p>
                                <div class="author-name">
                                    <h3><em>{{ $test->testimonal_name }}</em></h3>
                                    <p>{{ $test->testimonal_position }}</p>
                                </div>
                                <div class="author-pic"><img width="205" height="205"
                                                             src="{{ $test->testimonal_image }}" alt=""></div>
                            </div>
                        @endforeach
                    </div>
                </div>

                @endif
            </div>
        </div>

        <div class="clearfix"></div>

        <!--======= Gallery Section =========-->
        @if($event->event_image()->count() > 0)
        <div id="gallery">
            <div class="container">

                <div class="icon-main wow flipInX" data-wow-duration="1s" data-wow-delay="600ms"><i
                            class="ion-ios7-photos-outline"></i></div>

                <!--=======  Heading Tittle =========-->
                <div class="tittle wow flipInX" data-wow-duration="1s" data-wow-delay="800ms">
                    <h2>EventGO! <span>Startup Photo Gallery</span></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit eaque dolorem fugiat ullam ea!
                        Perspiciatis, ipsum ea possimus voluptatem sequi
                        amet harum dolores commodi eveniet earum est expedita error ab.</p>
                </div>

                <div class="col-md-12">
                    <ul class="row gallery-thump posts flex-container">
                        @foreach($event->event_image as $image)
                            <li class="col-md-2 flex-item">
                                <div class="inner-thump">
                                    <span class="img">
                                        <img style="height: 200px;width: 200px;" src="{{ $image->event_image }}" alt="">
                                    </span>
                                    <div class="over">
                                        <a href="{{ $image->event_image }}" data-rel="prettyPhoto"
                                           class="prettyPhoto lightzoom zoom"><i
                                                    class="ion-ios7-search-strong"></i>
                                        </a>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>

            </div>
        </div>
        @endif

        <!--======= Counter Timer =========-->
        <div id="timer">
            <div class="container">
                <div class="time">

                    <!-- Countdown-->
                    <ul class="row countdown">

                        <!--======= Days =========-->
                        <li class="col-sm-3">
                            <div class="round-back">
                                <span class="days">00</span>
                                <p class="days_ref">days</p>
                            </div>
                        </li>

                        <!--======= Houre =========-->
                        <li class="col-sm-3 ">
                            <div class="round-back">
                                <span class="hours">00</span>
                                <p class="hours_ref">hours</p>
                            </div>
                        </li>

                        <!--======= Mintes =========-->
                        <li class="col-sm-3">
                            <div class="round-back">
                                <span class="minutes">00</span>
                                <p class="minutes_ref">minutes</p>
                            </div>
                        </li>

                        <!--======= Seconds =========-->
                        <li class="col-sm-3">
                            <div class="round-back">
                                <span class="seconds">00</span>
                                <p class="seconds_ref">seconds</p>
                            </div>
                        </li>
                    </ul>
                    <!-- Countdown end-->
                </div>
            </div>
        </div>

        <!--=======  Member Startup Speakers =========-->
        @if($event->speaker()->count() > 0)
            <div id="member">
                <div class="container">
                    <div class="icon-main wow flipInX" data-wow-duration="1s" data-wow-delay="600ms"><i
                                class="ion-ios7-people-outline"></i>
                    </div>

                    <!--=======  Heading Tittle =========-->
                    <div class="tittle wow flipInX" data-wow-duration="1s" data-wow-delay="800ms">
                        <h2>EventGO! <span>Startup Speakers</span></h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit eaque dolorem fugiat ullam
                            ea!
                            Perspiciatis, ipsum ea possimus voluptatem sequi
                            amet harum dolores commodi eveniet earum est expedita error ab.</p>
                    </div>

                    <!--=======  Team Members =========-->
                    <div class="team">
                        <ul class="row grid flex-container">
                            @foreach($event->speaker as $speaker)
                                <li class="col-md-4 flex-item">
                                    <div class="figure effect-oscar"><img width="289" height="289"
                                                                          src="{{ $speaker->speaker_image }}" alt=""/>
                                        <div class="figcaption">
                                            <p class="flex-container">
                                                @if($speaker->facebook)
                                                    <a href="{{ $speaker->facebook }}" target="_blank" class="fb"> <span
                                                                class="ion-social-facebook"></span></a>
                                                @endif
                                                @if($speaker->twitter)
                                                    <a href="{{ $speaker->twitter }}" target="_blank" class="tw"> <span
                                                                class="ion-social-twitter"></span>
                                                    </a>
                                                @endif
                                                @if($speaker->linkin)
                                                    <a href="{{ $speaker->linkin }}" target="_blank"
                                                       class="linkedin"> <span
                                                                class="ion-social-linkedin-outline"></span> </a>
                                                @endif
                                            </p>
                                        </div>

                                        <div class="mem-detail">
                                            <div class="icon"><i class="ion-ios7-mic-outline"></i></div>
                                            <h4>{{ $speaker->speaker_name }}</h4>
                                            <p>{{ $speaker->speaker_position }}</p>
                                        </div>

                                    </div>
                                </li>
                            @endforeach
                        </ul>

                    </div>
                </div>
            </div>
        @endif

        @if($event->event_sponsor()->count() > 0 )
        <div id="partner">
            <div class="overlay">
                <div class="container">
                    <div class="icon-main wow flipInX" data-wow-duration="1s" data-wow-delay="600ms"><i
                                class="ion-ios7-people-outline"></i></div>

                    <div class="tittle wow flipInX" data-wow-duration="1s" data-wow-delay="800ms">
                        <h2>EventGO! <span>Event Partners</span></h2>
                    </div>
                </div>
            </div>

            <div class="partner">
                <div class="container">

                    <nav>
                        <ul class="nav nav-tabs nav-pills" role="tablist">
                            @foreach($event->event_sponsor()->groupBy('sponsor_type')->get() as $key => $sponsor)
                                <li @if($key == 0 ) class="active" @endif >
                                    <a href="#{{ $sponsor->sponsor_type }}" role="tab" data-toggle="tab">
                                        @if($sponsor->sponsor_type == 'silver')
                                            Silver Sponsors
                                        @elseif( $sponsor->sponsor_type == 'gold')
                                            Gold Sponsors
                                        @else
                                            Platinum Sponsors
                                        @endif
                                    </a>
                                </li>
                            @endforeach

                        </ul>
                    </nav>

                    <div class="tab-content">
                        @foreach($event->event_sponsor()->groupBy('sponsor_type')->get() as $key => $sponsor)
                            <div class="tab-pane fade @if($key == 0 ) in active @endif" id="{{ $sponsor->sponsor_type }}">
                                <div class="flex-container">
                                    @foreach($event->event_sponsor()->where('sponsor_type',$sponsor->sponsor_type )->get() as $key => $data)
                                    <div class="flex-item">
                                        <a href="{{ $data->sponsor_website_link }}">
                                            <img src="{{ $data->sponsor_image }}" alt="{{ $data->sponsor_name }}" style="height: 129px;width: 290px;">
                                        </a>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <br><br>
        @endif
        <div id="map">

        </div>

        <div class="social_icons">
            <div class="container">
                <ul class="flex-container">
                    <li class="flex-item facebook wow fadeInUpBig" data-wow-delay="100ms"><a
                                href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(request()->fullUrl()) }}"><i
                                    class="ion-social-facebook"></i> </a></li>
                    <li class="flex-item twitter wow fadeInUpBig" data-wow-delay="200ms"><a
                                href="https://twitter.com/intent/tweet?url={{ urlencode(request()->fullUrl()) }}"><i
                                    class="ion-social-twitter"></i> </a></li>
                    <li class="flex-item dribbble wow fadeInUpBig" data-wow-delay="300ms"><a
                                href="https://pinterest.com/pin/create/button/?{{
        http_build_query([
            'url' => request()->fullUrl(),
            'media' => $event->event_image()->first(),
            'description' => $event->description
        ])
        }}"><i
                                    class="ion-social-pinterest "></i> </a></li>
                    <li class="flex-item googleplus wow fadeInUpBig" data-wow-delay="400ms"><a
                                href="https://plus.google.com/share?url={{ urlencode(request()->fullUrl()) }}"><i
                                    class="ion-social-googleplus"></i> </a></li>
                    <li class="flex-item linkedin wow fadeInUpBig" data-wow-delay="500ms"><a
                                href="https://www.linkedin.com/shareArticle?mini=true&url={{ urlencode(request()->fullUrl()) }}&title={{ $event->title }}&summary={{ $event->description }}"><i
                                    class="ion-social-linkedin"></i> </a></li>
                </ul>
            </div>
        </div>

        @include('theme.default.layout.footer')

    </div>


@endsection

@section('script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfXOeoxQMK3CXttc2l-qSz1chohv9UXN4&libraries=weather,geometry,visualization,places,drawing&callback=map_pos"
            async defer></script>

    <script>

        var popupSize = {
            width: 780,
            height: 550
        };

        $(document).on('click', '.flex-container > .flex-item > a', function (e) {

            var
                    verticalPos = Math.floor(($(window).width() - popupSize.width) / 2),
                    horisontalPos = Math.floor(($(window).height() - popupSize.height) / 2);

            var popup = window.open($(this).prop('href'), 'social',
                    'width=' + popupSize.width + ',height=' + popupSize.height +
                    ',left=' + verticalPos + ',top=' + horisontalPos +
                    ',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1');

            if (popup) {
                popup.focus();
                e.preventDefault();
            }

        });
    </script>

    <script>
        $(function () {

            $('.countdown').downCount({
                date: '{{ date('m/d/Y',strtotime($event->event_date)) }}'
            });

        });
    </script>

    <script>
        function map_pos() {
            initMap({{ $event->lat }},{{ $event->long }} )
        }

        function initMap(mylat, mylong) {

            var myLatLng = {lat: mylat, lng: mylong};

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 14,
                center: myLatLng
            });

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map
            });

        }

        if ($('#map').length != 0) {
            google.maps.event.addDomListener(window, 'load', initMap);
        }
    </script>
@endsection