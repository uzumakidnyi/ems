@extends('theme.default.layout.main')

@section('content')
    <style>
        .jflatTimeline {
            width: 100%;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }

        .category_sidebar {
            width: 100%;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            background-color: #f10f24;
            display: table;
            width: 100%;
            color: #FFF;
            font-size: 25px;
            font-weight: 300;
            padding: 10px 0px 15px 0px;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            cursor: default;
            margin-top: 33px;
            border-radius: 5px 5px 0px 0px;

        }

        .jflatTimeline .month-year-bar {
            background-color: #f10f24;
            display: table;
            width: 100%;
            color: #FFF;
            font-size: 25px;
            font-weight: 300;
            padding: 10px 0px 15px 0px;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            cursor: default;
            margin-top: 33px;
            border-radius: 5px 5px 0px 0px;
        }

        .jflatTimeline .dates-bar {
            border: solid 1px #e7e7e7;
            display: block;
            width: 100%;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            padding: 0 50px;
            position: relative;
            font-size: 0;
            white-space: nowrap;
            overflow: hidden;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border-radius: 0px;
            margin-top: 0px;
            background: #FFF;
        }

        .jflatTimeline .month-year-bar .prev, .jflatTimeline .month-year-bar .next {
            padding: 0 12px;
            font-size: 30px;
            color: #FFF;
            cursor: pointer;
        }

        .jflatTimeline .dates-bar a span.date {
            display: block;
            font-size: 30px;
            padding: 5px;
        }

        .jflatTimeline .dates-bar a {
            display: block;
            height: 90px;
            width: 80px;
            color: #000;
            text-align: center;
            display: inline-block;
            border-right: 1px solid #E7E7E7;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            cursor: pointer;
            transition: color .2s, transform .2s;
            -webkit-transition: color .2s, transform .2s;
            -moz-transition: color .2s, transform .2s;
            z-index: 0;
        }

        .jflatTimeline .dates-bar a.prev, .jflatTimeline .dates-bar a.next {
            position: absolute;
            top: 0px;
            width: 50px;
            height: 100%;
            min-width: 0;
            font-size: 20px;
            background-color: white;
            font-size: 30px;
            line-height: 85px;
            z-index: 2;
            display: inline-block;
        }

        .jflatTimeline .dates-bar a.selected {
            background-color: transparent;
            color: #f10f24;
            font-weight: 700;
        }

        .jflatTimeline .dates-bar a:hover {
            color: #f10f24;
            text-decoration: none;
        }

        .jflatTimeline .dates-bar a.noevent {
            display: none;
            width: 100%;
            color: #7B7B7B;
            font-size: 19px;
            line-height: 70px;
            padding: 10px;
        }

    </style>

    <img src="{{ url('theme/default/images/bg_slider_2.jpg') }}"
         style="margin-top: -15%;z-index: 990;position: fixed;width: 100%;height: 70%;" alt="EventGo"
         data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">


    <div id="content" xmlns="http://www.w3.org/1999/html">

        <div class="container" style="z-index: 991;position: relative;margin-top: 20px">
            <div class="row">
                <div class="col-md-8">
                    @if(count($event) > 0)
                        <div class="jflatTimeline">
                            <div class="owl-testi timeline-wrap" style="margin-top: 0px">
                                <?php $i = 200;$x = 1  ?>
                                @foreach($event as $event_list)
                                    <div @if(date('m/d/Y' , strtotime($event_list->event_date)) == date('m/d/Y' , strtotime(\Carbon\Carbon::now()))) class="selected event"
                                         @else class="event"
                                         @endif data-date="{{ date('m/d/Y' , strtotime($event_list->event_date)) }}">
                                        <div class="testi wow flipInX" data-wow-duration="1s"
                                             data-wow-delay="{{ $i }}ms">
                                            <h5>
                                                <i class="ion-ios7-clock"></i>&nbsp;&nbsp;Event Date
                                                : {{ date('F j, Y' , strtotime($event_list->event_date)) }}

                                                @if($event_list->event_time()->orderBy('event_date','desc')->first()->event_date != $event_list->event_date)
                                                    -
                                                    {{ date('F j, Y' , strtotime($event_list->event_time()->orderBy('event_date','desc')->first()->event_date )) }}
                                                @endif

                                                @if( $event_list->complete == 1 )
                                                    <div class="e-time"
                                                         style="float: right;width: auto;padding: 0px 10px 0px 10px;">
                                                        <span>Complete</span>
                                                    </div>
                                                @endif
                                            </h5>
                                            <h2>{{ $event_list->title }}
                                                <small style="font-size: 60%">{{ $event_list->sub_title }}</small>
                                            </h2>
                                            <p>{{ $event_list->description }}</p>
                                            <br>
                                            <div class="author-name">
                                                <h3><em>{{ $event_list->city }}</em></h3>
                                                <p>{{ $event_list->address }}</p>
                                                <p>
                                                    <i class="ion-ios7-telephone"></i>&nbsp;&nbsp;{{ $event_list->contact_number }}
                                                </p>
                                            </div>

                                            @if(count($event_list->event_category()->get()) > 0)
                                                <br>
                                                <div class="row" style="padding-left: 15px;">
                                                    @foreach($event_list->event_category()->get() as $category)
                                                        <div class="e-time"
                                                             style="width: auto;padding: 0px 5px 0px 5px;">
                                                            {{ $category->category_name }}
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <br>
                                            @endif

                                            <div class="row">
                                                <div class="col-md-6 col-md-offset-6">
                                                    <ul class="event_countdown"
                                                        data-countdown="{{ date('m/d/Y',strtotime($event_list->event_date)) }}"
                                                        data-id="{{ $event_list->id }}">

                                                        <!--======= Days =========-->
                                                        <li class="col-sm-3">

                                                                <span class="days"
                                                                      style="font-size: 18px;font-weight: 500;color: #FFF">00</span>
                                                            <p class="days_ref">days</p>

                                                        </li>

                                                        <!--======= Houre =========-->
                                                        <li class="col-sm-3">

                                                            <span class="hours"
                                                                  style="font-size: 18px;font-weight: 500;color: #FFF">00</span>
                                                            <p class="hours_ref">hours</p>

                                                        </li>

                                                        <!--======= Mintes =========-->
                                                        <li class="col-sm-3">

                                                            <span class="minutes"
                                                                  style="font-size: 18px;font-weight: 500;color: #FFF">00</span>
                                                            <p class="minutes_ref">minutes</p>

                                                        </li>

                                                        <!--======= Seconds =========-->
                                                        <li class="col-sm-3">

                                                            <span class="seconds"
                                                                  style="font-size: 18px;font-weight: 500;color: #FFF">00</span>
                                                            <p class="seconds_ref">seconds</p>

                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                            <?php $title = \Illuminate\Support\Str::slug($event_list->title) ?>
                                            <div class="author-name">

                                                <a class="btn" style="width: auto"
                                                   href="{{ url('event-detail').'/'.$title.'?id='.$event_list->id }}">
                                                    More </a>
                                                @if(\Auth::check())
                                                    <a class="btn" style="width: auto"
                                                       href="#" onclick="get_ticket({{ $event_list->id  }})">
                                                        Get Event Ticket </a>
                                                @else
                                                    <a class="btn plz" style="width: 145px"
                                                       href="#" onclick="plz_register()">
                                                        Get Event Ticket </a>
                                                @endif


                                            </div>
                                        </div>

                                        <?php $i += 200;$x++  ?>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @else
                        <div class="category_sidebar">
                            <div class="owl-testi" style="margin-top: 0px">
                                <div class="testi wow fadeInLeft">
                                    <h2 style="color: #FFF;padding: 50px 0px 0px 0px;text-align: center;">No Event Found
                                        :( </h2>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-md-4">
                    <div class="category_sidebar">
                        <div class="owl-testi" style="margin-top: 0px">
                            <h4 style="color: #FFF;padding: 6px 0px 0px 21px;">Filter Events By Category</h4>
                            <div class="testi wow fadeInLeft">
                                <div class="e-time" style="width: auto;padding: 0px 5px 0px 5px;">
                                    <a href="{{ url('event-list') }}">All</a>
                                </div>
                                @foreach($categories as $category)
                                    <div class="e-time" style="width: auto;padding: 0px 5px 0px 5px;">
                                        <a href="{{ url('event-list').'?category='.\Illuminate\Support\Str::slug($category->category_name).'&id='.$category->id }}">{{ $category->category_name }}</a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            @include('theme.default.layout.footer')

        </div>
        @endsection

        @section('script')

            <script>
                $(function () {

                    $('.event_countdown').each(function () {
                        var eventdate = $(this).attr("data-countdown");
                        var id = $(this).attr("data-id");
                        $(this).downCount({
                            date: eventdate
                        }, function () {
                            $.ajax({
                                type: 'post',
                                url: '{{ url('complete_event') }}',
                                data: {
                                    'id': id
                                },
                                success: function (data) {

                                },
                                error: function (data) {

                                }
                            }, "json");
                        });
                    });

                });
            </script>


            <script>
                function plz_register() {
                    swal({
                        type: 'info',
                        title: 'Please create account ! and get event ticket !',
                        html: '<a href="{{ url('login') }}" class="largeredbtn">Register Here !</a> ',
                        showConfirmButton: false
                    });
                }


                function get_ticket(id) {


                    swal({
                        title: "Get Ticket!",
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        cancelButtonText: 'No',
                        confirmButtonClass: 'btn',
                        cancelButtonClass: 'btn',
                        buttonsStyling: false
                    }).then(function () {
                        $.ajax({
                            type: 'get',
                            url: '{{ url('member/get_ticket')}}' + '/' + id,
                            data: '',
                            dataType: 'json',
                            success: function (data) {
                                swal({
                                    type: 'success',
                                    title: 'Ticket Create Successful',
                                    html: 'Please check your ticket list!',
                                    showConfirmButton: false
                                });
                            },
                            error: function (data) {
                                swal({
                                    type: 'error',
                                    title: 'Oops !',
                                    html: 'Seat Full',
                                    showConfirmButton: false
                                });
                            }
                        });
                    })
                }
            </script>
@endsection