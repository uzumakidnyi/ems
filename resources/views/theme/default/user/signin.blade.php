@extends('theme.default.layout.main')
@section('content')

    <div id="content" xmlns="http://www.w3.org/1999/html" >

        <div class="container" style="z-index: 991;position: relative;margin-top: -40px">
            <div class="row">
                <div class="col-md-6">
                    <div class="owl-testi timeline-wrap">
                        <div class="testi wow flipInX" data-wow-duration="1s" data-wow-delay="200ms">
                            <form role="form" action="/login" method="post">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="left-align"><i class="ion ion-ios-search"></i>&nbsp;&nbsp;E v e n t G
                                            O !&nbsp;&nbsp;
                                            <small class="right-align">Member Login</small>
                                        </h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="email" name="email" value="{{ old('email') }}"
                                                   class="form-control" placeholder="E-mail Adress">
                                        </div>
                                    </div>
                                    @if ($errors->has('email'))
                                        <h5 class="center-align" style="padding: 5px 0px 0px 15px;"><a
                                                    style="color: #FFF;" class="waves-attach"
                                                    href="#">{{ $errors->first('email') }}</a></h5>
                                    @endif
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="password" name="password" class="form-control"
                                                   placeholder="Password">
                                        </div>
                                    </div>
                                    @if ($errors->has('password'))
                                        <h5 class="center-align" style="padding: 5px 0px 0px 15px;"><a
                                                    style="color: #FFF;" class="waves-attach"
                                                    href="#">{{ $errors->first('password') }}</a></h5>
                                    @endif
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <button type="submit" style="width: 105px" class="btn">LOGIN!</button>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <a href="{{ url('login/facebook') }}" style="width: 105px" class="btn"><i
                                                        class="fa fa-facebook"></i>&nbsp;&nbsp; Facebook</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <a href="{{ url('login/google') }}" style="width: 105px" class="btn"><i
                                                        class="fa fa-google"></i>&nbsp;&nbsp; Google</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <a href="{{ url('login/twitter') }}" style="width: 105px" class="btn"><i
                                                        class="fa fa-twitter"></i>&nbsp;&nbsp; Twitter</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <h5 class="center-align" style="padding: 5px 0px 0px 15px;"><a style="color: #FFF;"
                                                                                                   id="forgot"
                                                                                                   class="waves-attach"
                                                                                                   href="#">Forgot
                                            Password?</a></h5>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="owl-testi timeline-wrap">
                        <div class="testi wow flipInX" data-wow-duration="1s" data-wow-delay="400ms">
                            <form role="form" action="/register" method="post">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="left-align"><i class="ion ion-ios-search"></i>&nbsp;&nbsp;E v e n t G
                                            O !&nbsp;&nbsp;
                                            <small class="right-align">Member Registration Form</small>
                                        </h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" name="name" value="{{ old('name') }}"
                                                   class="form-control" placeholder="Enter Name">
                                        </div>
                                    </div>
                                    @if ($errors->has('name'))
                                        <h5 class="center-align" style="padding: 5px 0px 0px 15px;"><a
                                                    style="color: #FFF;" class="waves-attach"
                                                    href="#">{{ $errors->first('name') }}</a></h5>
                                    @endif
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="email" name="email" value="{{ old('email') }}"
                                                   class="form-control" placeholder="E-mail Adress">
                                        </div>
                                    </div>
                                    @if ($errors->has('email'))
                                        <h5 class="center-align" style="padding: 5px 0px 0px 15px;"><a
                                                    style="color: #FFF;" class="waves-attach"
                                                    href="#">{{ $errors->first('email') }}</a></h5>
                                    @endif
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="password" name="password" class="form-control"
                                                   placeholder="Password">
                                        </div>
                                    </div>
                                    @if ($errors->has('password'))
                                        <h5 class="center-align" style="padding: 5px 0px 0px 15px;"><a
                                                    style="color: #FFF;" class="waves-attach"
                                                    href="#">{{ $errors->first('password') }}</a></h5>
                                    @endif
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="password" name="password_confirmation" class="form-control"
                                                   placeholder="Re-type Password">
                                        </div>
                                    </div>
                                    @if ($errors->has('password_confirmation'))
                                        <h5 class="center-align" style="padding: 5px 0px 0px 15px;"><a
                                                    style="color: #FFF;" class="waves-attach"
                                                    href="#">{{ $errors->first('password_confirmation') }}</a></h5>
                                    @endif
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <button type="submit" style="width: 125px" class="btn">LOGIN!</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        @include('theme.default.layout.footer')

    </div>
@endsection
@section('script')
    <script>
        $('#forgot').on('click', function (e) {

            swal({
                title: 'Enter your email!',
                width: 500,
                padding: 30,
                background: '#fff url(//bit.ly/1Nqn9HU)',
                input: 'email',
                showCancelButton: false,
                confirmButtonText: 'Submit',
                confirmButtonClass: 'form-control',
                showLoaderOnConfirm: true,
                preConfirm: function (email) {
                    return new Promise(function (resolve, reject) {
                        setTimeout(function () {
                            if (email) {
                                $.ajax({
                                    type: 'POST',
                                    url: '{{ url('checkemail')}}',
                                    data: {
                                        email: email
                                    },
                                    dataType: 'json',
                                    success: function (data) {
                                        $.ajax({
                                            type: 'POST',
                                            url: '{{ url('password/email')}}',
                                            data: {
                                                email: email
                                            },
                                            dataType: 'json',
                                            success: function (data) {
                                                resolve();
                                            },
                                            error: function (data) {
                                                resolve();
                                            }
                                        });
                                    },
                                    error: function (data) {
                                        reject('We can\'t find a user with that e - mail address.');
                                    }
                                });
                            } else {
                                reject('Please enter password!');
                            }
                        }, 2000);
                    });
                },
                allowOutsideClick: true
            }).then(function (email) {
                swal({
                    type: 'success',
                    title: 'Request Complete !',
                    html: 'Check your inbox, to get Password Reset Link !'
                });
            });

        });

    </script>
@endsection