@extends('theme.default.layout.main')

@section('content')
    <div id="content" xmlns="http://www.w3.org/1999/html">
        <div class="container" style="margin-top: -40px">
            <div class="row">
                <div class="col-md-3">
                    <div class="owl-testi">
                        <div class="testi">
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4" style="margin-left: 53%">
                                    <div class="author-pic">
                                        <img src="{{ Auth::user()->avatar }}" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12" style="padding: 10%;text-align: center;">
                                    <p>{{ Auth::user()->name }}</p>
                                    <p>{{ Auth::user()->email }}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <nav>
                        <ul class="nav nav-tabs nav-pills" role="tablist">
                            <li style="width:100%;" class="active"><a href="#account_info" role="tab" data-toggle="tab">Profile</a></li>
                            <li style="width:100%;"><a href="#ticket_list" role="tab" data-toggle="tab">Ticket List</a></li>
                            <li style="width:100%;"><a href="#friend_list" role="tab" data-toggle="tab">Friend List</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-md-9">
                    <div class="days wow bounceIn" data-wow-duration="1s" data-wow-delay="300ms">

                        <!-- Nav tabs -->

                        <!-- Tab panes -->
                        <div class="tab-content">

                            <!--==========Days 1 Start==========-->
                            <div class="tab-pane fade in active" id="account_info">
                                <!-- Nav tabs -->
                                <nav>
                                    <ul class="nav nav-tabs nav-pills" role="tablist">
                                        <li class="active"><a href="#Day-1" role="tab" data-toggle="tab">Account Details</a></li>
                                        <li><a href="#Day-2" role="tab" data-toggle="tab">Change Password</a></li>
                                        <li><a href="#Day-3" role="tab" data-toggle="tab">Setting</a></li>
                                    </ul>
                                </nav>
                                <!-- Tab panes -->
                                <div class="tab-content">

                                    <!--==========Days 1 Start==========-->
                                    <div class="tab-pane fade in active" id="Day-1">
                                        <p>It is a long established fact that a reader will be distracted by the readable content.</p>
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">

                                                    <!--=======  Panel Details =========-->
                                                    <div class="panel-title">
                                                        <div class="e-time"><i class="ion-ios7-clock"></i> 09:30</div>
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                            <p>Lorem Ipsum is simply dummy text.</p>
                                                        </a></div>
                                                </div>

                                                <!--=======  collapseOne =========-->
                                                <div id="collapseOne" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                            Lorem Ipsum has been the industry's standard dummy text ever since the
                                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                                            make a type specimen book. <br>
                                                            <br>
                                                            It has survived not only five centuries, but also the leap into electronic
                                                            typesetting, remaining essentially unchanged. </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--=======  Panel Details =========-->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <div class="panel-title">
                                                        <div class="e-time"><i class="ion-ios7-clock"></i> 11:20</div>
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"
                                                           class="collapsed">
                                                            <p>Lorem Ipsum is simply dummy text.</p>
                                                        </a></div>
                                                </div>

                                                <!--=======  collapseTwo =========-->
                                                <div id="collapseTwo" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                            Lorem Ipsum has been the industry's standard dummy text ever since the
                                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                                            make a type specimen book. <br>
                                                            <br>
                                                            It has survived not only five centuries, but also the leap into electronic
                                                            typesetting, remaining essentially unchanged. </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--=======  Panel Details =========-->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <div class="panel-title">
                                                        <div class="e-time"><i class="ion-ios7-clock"></i> 13:00</div>
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"
                                                           class="collapsed">
                                                            <p>Lorem Ipsum is simply dummy text.</p>
                                                        </a></div>
                                                </div>

                                                <!--=======  collapseThree =========-->
                                                <div id="collapseThree" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                            Lorem Ipsum has been the industry's standard dummy text ever since the
                                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                                            make a type specimen book. <br>
                                                            <br>
                                                            It has survived not only five centuries, but also the leap into electronic
                                                            typesetting, remaining essentially unchanged. </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--=======  Panel Details =========-->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <div class="panel-title">
                                                        <div class="e-time"><i class="ion-ios7-clock"></i> 14:20</div>
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour"
                                                           class="collapsed">
                                                            <p>Lorem Ipsum is simply dummy text.</p>
                                                        </a></div>
                                                </div>

                                                <!--=======  collapsefour =========-->
                                                <div id="collapsefour" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                            Lorem Ipsum has been the industry's standard dummy text ever since the
                                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                                            make a type specimen book. <br>
                                                            <br>
                                                            It has survived not only five centuries, but also the leap into electronic
                                                            typesetting, remaining essentially unchanged. </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!--==========Days 2 Start==========-->
                                    <div class="tab-pane fade" id="Day-2">
                                        <p>It is a long established fact that a reader will be distracted by the readable content.</p>
                                        <div class="panel-group" id="accordion1">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <div class="panel-title">
                                                        <div class="e-time"><i class="ion-ios7-clock"></i> 10:30</div>
                                                        <a data-toggle="collapse" data-parent="#accordion1" href="#collapseFive">
                                                            <p>Lorem Ipsum is simply dummy text.</p>
                                                        </a></div>
                                                </div>

                                                <!--=======  collapseFive =========-->
                                                <div id="collapseFive" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                            Lorem Ipsum has been the industry's standard dummy text ever since the
                                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                                            make a type specimen book. <br>
                                                            <br>
                                                            It has survived not only five centuries, but also the leap into electronic
                                                            typesetting, remaining essentially unchanged. </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--=======  Panel Details =========-->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <div class="panel-title">
                                                        <div class="e-time"><i class="ion-ios7-clock"></i> 12:20</div>
                                                        <a data-toggle="collapse" data-parent="#accordion1" href="#collapseSix"
                                                           class="collapsed">
                                                            <p>Lorem Ipsum is simply dummy text.</p>
                                                        </a></div>
                                                </div>

                                                <!--=======  collapseSix =========-->
                                                <div id="collapseSix" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                            Lorem Ipsum has been the industry's standard dummy text ever since the
                                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                                            make a type specimen book. <br>
                                                            <br>
                                                            It has survived not only five centuries, but also the leap into electronic
                                                            typesetting, remaining essentially unchanged. </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--=======  Panel Details =========-->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <div class="panel-title">
                                                        <div class="e-time"><i class="ion-ios7-clock"></i> 14:00</div>
                                                        <a data-toggle="collapse" data-parent="#accordion1" href="#collapseSeven"
                                                           class="collapsed">
                                                            <p>Lorem Ipsum is simply dummy text.</p>
                                                        </a></div>
                                                </div>

                                                <!--=======  collapseSeven =========-->
                                                <div id="collapseSeven" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                            Lorem Ipsum has been the industry's standard dummy text ever since the
                                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                                            make a type specimen book. <br>
                                                            <br>
                                                            It has survived not only five centuries, but also the leap into electronic
                                                            typesetting, remaining essentially unchanged. </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--=======  Panel Details =========-->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <div class="panel-title">
                                                        <div class="e-time"><i class="ion-ios7-clock"></i> 15:20</div>
                                                        <a data-toggle="collapse" data-parent="#accordion1" href="#collapseEight"
                                                           class="collapsed">
                                                            <p>Lorem Ipsum is simply dummy text.</p>
                                                        </a></div>
                                                </div>

                                                <!--=======  collapseEight =========-->
                                                <div id="collapseEight" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                            Lorem Ipsum has been the industry's standard dummy text ever since the
                                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                                            make a type specimen book. <br>
                                                            <br>
                                                            It has survived not only five centuries, but also the leap into electronic
                                                            typesetting, remaining essentially unchanged. </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!--==========Days 3 Start==========-->
                                    <div class="tab-pane fade" id="Day-3">
                                        <p>It is a long established fact that a reader will be distracted by the readable content.</p>
                                        <div class="panel-group" id="accordion2">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <div class="panel-title">
                                                        <div class="e-time"><i class="ion-ios7-clock"></i> 16:30</div>
                                                        <a data-toggle="collapse" data-parent="#accordion2" href="#collapseNine">
                                                            <p>Lorem Ipsum is simply dummy text.</p>
                                                        </a></div>
                                                </div>

                                                <!--=======  collapseNine =========-->
                                                <div id="collapseNine" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                            Lorem Ipsum has been the industry's standard dummy text ever since the
                                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                                            make a type specimen book. <br>
                                                            <br>
                                                            It has survived not only five centuries, but also the leap into electronic
                                                            typesetting, remaining essentially unchanged. </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--=======  Panel Details =========-->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <div class="panel-title">
                                                        <div class="e-time"><i class="ion-ios7-clock"></i> 17:20</div>
                                                        <a data-toggle="collapse" data-parent="#accordion2" href="#collapseTen"
                                                           class="collapsed">
                                                            <p>Lorem Ipsum is simply dummy text.</p>
                                                        </a></div>
                                                </div>

                                                <!--=======  collapseTen =========-->
                                                <div id="collapseTen" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                            Lorem Ipsum has been the industry's standard dummy text ever since the
                                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                                            make a type specimen book. <br>
                                                            <br>
                                                            It has survived not only five centuries, but also the leap into electronic
                                                            typesetting, remaining essentially unchanged. </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--=======  Panel Details =========-->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <div class="panel-title">
                                                        <div class="e-time"><i class="ion-ios7-clock"></i> 18:00</div>
                                                        <a data-toggle="collapse" data-parent="#accordion2" href="#collapseEleven"
                                                           class="collapsed">
                                                            <p>Lorem Ipsum is simply dummy text.</p>
                                                        </a></div>
                                                </div>

                                                <!--=======  collapseEleven =========-->
                                                <div id="collapseEleven" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                            Lorem Ipsum has been the industry's standard dummy text ever since the
                                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                                            make a type specimen book. <br>
                                                            <br>
                                                            It has survived not only five centuries, but also the leap into electronic
                                                            typesetting, remaining essentially unchanged. </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--=======  Panel Details =========-->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <div class="panel-title">
                                                        <div class="e-time"><i class="ion-ios7-clock"></i> 19:20</div>
                                                        <a data-toggle="collapse" data-parent="#accordion2" href="#collapseTwelve"
                                                           class="collapsed">
                                                            <p>Lorem Ipsum is simply dummy text.</p>
                                                        </a></div>
                                                </div>

                                                <!--=======  collapseTwelve =========-->
                                                <div id="collapseTwelve" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                            Lorem Ipsum has been the industry's standard dummy text ever since the
                                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                                            make a type specimen book. <br>
                                                            <br>
                                                            It has survived not only five centuries, but also the leap into electronic
                                                            typesetting, remaining essentially unchanged. </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--==========Days 2 Start==========-->
                            <div class="tab-pane fade" id="ticket_list">
                                <div class="owl-testi" style="margin-top: 0%">
                                    <?php $i = 200 ?>
                                    @foreach($member as $event_list)
                                            <div class="testi wow flipInX" data-wow-duration="1s" data-wow-delay="{{ $i }}ms">
                                                <h5><i class="ion-ios7-clock"></i>&nbsp;&nbsp;Event Date
                                                    - {{ date('F j, Y' , strtotime($event_list->event_date)) }}  | Ticket Number
                                                    - {{ $event_list->pivot->ticket_number }} </h5>
                                                <br>
                                                <h3>{{ $event_list->title }}</h3>
                                                <br>
                                                <div class="author-name">

                                                        <a class="btn" style="width: 145px"
                                                           href="#">
                                                            Download Ticket </a>

                                                        <a class="btn plz" style="width: 222px"
                                                           href="#" onclick="get_qr_code({{ $event_list->id  }})">
                                                            Download Ticket as QR Code</a>

                                                </div>
                                            </div>
                                        <?php $i += 200 ?>
                                    @endforeach
                                </div>
                            </div>

                            <!--==========Days 3 Start==========-->
                            <div class="tab-pane fade" id="friend_list">
                                <p>It is a long established fact that a reader will be distracted by the readable content.</p>
                                <div class="panel-group" id="accordion2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <div class="e-time"><i class="ion-ios7-clock"></i> 16:30</div>
                                                <a data-toggle="collapse" data-parent="#accordion2" href="#collapseNine">
                                                    <p>Lorem Ipsum is simply dummy text.</p>
                                                </a></div>
                                        </div>

                                        <!--=======  collapseNine =========-->
                                        <div id="collapseNine" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                    Lorem Ipsum has been the industry's standard dummy text ever since the
                                                    1500s, when an unknown printer took a galley of type and scrambled it to
                                                    make a type specimen book. <br>
                                                    <br>
                                                    It has survived not only five centuries, but also the leap into electronic
                                                    typesetting, remaining essentially unchanged. </p>
                                            </div>
                                        </div>
                                    </div>

                                    <!--=======  Panel Details =========-->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <div class="e-time"><i class="ion-ios7-clock"></i> 17:20</div>
                                                <a data-toggle="collapse" data-parent="#accordion2" href="#collapseTen"
                                                   class="collapsed">
                                                    <p>Lorem Ipsum is simply dummy text.</p>
                                                </a></div>
                                        </div>

                                        <!--=======  collapseTen =========-->
                                        <div id="collapseTen" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                    Lorem Ipsum has been the industry's standard dummy text ever since the
                                                    1500s, when an unknown printer took a galley of type and scrambled it to
                                                    make a type specimen book. <br>
                                                    <br>
                                                    It has survived not only five centuries, but also the leap into electronic
                                                    typesetting, remaining essentially unchanged. </p>
                                            </div>
                                        </div>
                                    </div>

                                    <!--=======  Panel Details =========-->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <div class="e-time"><i class="ion-ios7-clock"></i> 18:00</div>
                                                <a data-toggle="collapse" data-parent="#accordion2" href="#collapseEleven"
                                                   class="collapsed">
                                                    <p>Lorem Ipsum is simply dummy text.</p>
                                                </a></div>
                                        </div>

                                        <!--=======  collapseEleven =========-->
                                        <div id="collapseEleven" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                    Lorem Ipsum has been the industry's standard dummy text ever since the
                                                    1500s, when an unknown printer took a galley of type and scrambled it to
                                                    make a type specimen book. <br>
                                                    <br>
                                                    It has survived not only five centuries, but also the leap into electronic
                                                    typesetting, remaining essentially unchanged. </p>
                                            </div>
                                        </div>
                                    </div>

                                    <!--=======  Panel Details =========-->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <div class="e-time"><i class="ion-ios7-clock"></i> 19:20</div>
                                                <a data-toggle="collapse" data-parent="#accordion2" href="#collapseTwelve"
                                                   class="collapsed">
                                                    <p>Lorem Ipsum is simply dummy text.</p>
                                                </a></div>
                                        </div>

                                        <!--=======  collapseTwelve =========-->
                                        <div id="collapseTwelve" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                    Lorem Ipsum has been the industry's standard dummy text ever since the
                                                    1500s, when an unknown printer took a galley of type and scrambled it to
                                                    make a type specimen book. <br>
                                                    <br>
                                                    It has survived not only five centuries, but also the leap into electronic
                                                    typesetting, remaining essentially unchanged. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        @include('theme.default.layout.footer')

    </div>
@endsection

@section('script')
    <script>
        function get_qr_code(id) {

            $.ajax({
                type: 'get',
                url: '{{ url('member/get_qr_code')}}'+'/'+id ,
                data: '',
                success: function (data) {
                    swal({
                        imageUrl :data ,
                        imageWidth: 400,
                        imageHeight: 400,
                        width: 400,
                        animation: false,
                        html:'<a href="'+ data +'" download="event_ticket.jpg">Download</a>',
                        showConfirmButton: false
                    });
                },
                error: function (data) {
                    swal({
                        type: 'error',
                        title: 'Oops !',
                        showConfirmButton: false
                    });
                }
            });

        }
    </script>
@endsection