<div id="banner">
    <div class="tp-banner-container">
        <div class="tp-banner" >
            <ul>
                <!-- SLIDE  -->
                <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="{{ url('theme/default/images/bg_slider_1.jpg') }}"  data-saveperformance="off"  data-title="Slide">
                    <!-- MAIN IMAGE -->
                    <img src="{{ url('theme/default/images/bg_slider_1.jpg') }}"  alt="fullslide1"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

                    <!-- LAYERS -->
                    <div class="tp-caption light_medium_20 tp-fade fadeout tp-resizeme"
                         data-x="left" data-hoffset="40"
                         data-y="center" data-voffset="-60"
                         data-speed="500"
                         data-start="500"
                         data-easing="Power4.easeOut"
                         data-splitin="chars"
                         data-splitout="chars"
                         data-elementdelay="0.05"
                         data-endelementdelay="0.05"
                         data-endspeed="300"
                         style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">NEW - AUGUST </div>

                    <!-- LAYERS -->
                    <div class="tp-caption white_heavy_70 tp-fade fadeout tp-resizeme"
                         data-x="left" data-hoffset="40"
                         data-y="center" data-voffset="0"
                         data-speed="500"
                         data-start="500"
                         data-easing="Power4.easeOut"
                         data-splitin="chars"
                         data-splitout="chars"
                         data-elementdelay="0.05"
                         data-endelementdelay="0.05"
                         data-endspeed="300"
                         style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">Business Freelancer <span>Corporate Events </span></div>

                    <!-- LAYERS -->
                    <div class="tp-caption white_heavy_48_light tp-fade fadeout tp-resizeme"
                         data-x="left" data-hoffset="40"
                         data-y="center" data-voffset="25"
                         data-speed="700"
                         data-start="700"
                         data-easing="Power4.easeOut"
                         data-splitin="chars"
                         data-splitout="chars"
                         data-elementdelay="0.05"
                         data-endelementdelay="0.05"
                         data-endspeed="300"
                         style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;"> Best Seller Conferance...</div>

                    <!-- LAYERS -->
                    <div class="tp-caption black_thin_30 tp-fade fadeout tp-resizeme"
                         data-x="left" data-hoffset="40"
                         data-y="center" data-voffset="100"
                         data-speed="700"
                         data-start="700"
                         data-easing="Power4.easeOut"
                         data-splitin="chars"
                         data-splitout="chars"
                         data-elementdelay="0.05"
                         data-endelementdelay="0.05"
                         data-endspeed="300"
                         style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;"> Popular Business Ceo Conferance</div>

                    <!-- LAYERS -->
                    <div class="tp-caption customin tp-resizeme rs-parallaxlevel-0"
                         data-x="left" data-hoffset="40"
                         data-y="center" data-voffset="200"
                         data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="1000"
                         data-start="1000"
                         data-easing="Power3.easeInOut"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.1"
                         data-endelementdelay="0.1"
                         data-linktoslide="next"
                         style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;"><a href='#' class='largeredbtn'>REGISTER</a> </div>

                    <!-- LAYERS -->
                    <div class="tp-caption customin tp-resizeme rs-parallaxlevel-0"
                         data-x="left" data-hoffset="280"
                         data-y="center" data-voffset="200"
                         data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="1200"
                         data-start="1200"
                         data-easing="Power3.easeInOut"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.1"
                         data-endelementdelay="0.1"
                         data-linktoslide="next"
                         style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;"><a href='#' class='largeredbtn'>LEARN MORE</a> </div>
                </li>
                <!-- SLIDE  -->

                <li data-transition="slidevertical" data-slotamount="1" data-masterspeed="1000" data-thumb="images/thumb-2.jpg"  data-saveperformance="off"  data-title="Slide">
                    <!-- MAIN IMAGE -->
                    <img src="{{ url('theme/default/images/bg_slider_2.jpg') }}"  alt="EventGo"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

                    <!-- LAYERS -->
                    <div class="tp-caption lfb ltt tp-resizeme"
                         data-x="left" data-hoffset="100"
                         data-y="center" data-voffset="60"
                         data-speed="800"
                         data-start="600"
                         data-easing="Power4.easeOut"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="500"
                         data-endeasing="Power4.easeIn"
                         style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"> <img src="{{ url('theme/default/images/map-icon-banner.png') }}" alt=""> </div>

                    <!-- LAYERS -->
                    <div class="tp-caption white_heavy_70 tp-fade fadeout tp-resizeme"
                         data-x="left" data-hoffset="400"
                         data-y="center" data-voffset="0"
                         data-speed="500"
                         data-start="500"
                         data-easing="Power4.easeOut"
                         data-splitin="chars"
                         data-splitout="chars"
                         data-elementdelay="0.05"
                         data-endelementdelay="0.05"
                         data-endspeed="300"
                         style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">Corporate Freelancer Events</div>

                    <!-- LAYERS -->
                    <div class="tp-caption black_thin_30 tp-fade fadeout tp-resizeme"
                         data-x="left" data-hoffset="400"
                         data-y="center" data-voffset="40"
                         data-speed="700"
                         data-start="700"
                         data-easing="Power4.easeOut"
                         data-splitin="chars"
                         data-splitout="chars"
                         data-elementdelay="0.05"
                         data-endelementdelay="0.05"
                         data-endspeed="300"
                         style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;"> Popular Business Ceo Conferance</div>

                    <!-- LAYERS -->
                    <div class="tp-caption customin tp-resizeme rs-parallaxlevel-0"
                         data-x="left" data-hoffset="400"
                         data-y="center" data-voffset="130"
                         data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="1000"
                         data-start="1000"
                         data-easing="Power3.easeInOut"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.1"
                         data-endelementdelay="0.1"
                         data-linktoslide="next"
                         style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;"><a href='#' class='largeredbtn'>REGISTER</a> </div>

                    <!-- LAYERS -->
                    <div class="tp-caption customin tp-resizeme rs-parallaxlevel-0"
                         data-x="left" data-hoffset="680"
                         data-y="center" data-voffset="130"
                         data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="1200"
                         data-start="1200"
                         data-easing="Power3.easeInOut"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.1"
                         data-endelementdelay="0.1"
                         data-linktoslide="next"
                         style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;"><a href='#' class='largeredbtn'>LEARN MORE</a> </div>
                </li>
                <!-- SLIDE  -->

            </ul>
            <div class="tp-bannertimer"></div>
        </div>
    </div>
</div>