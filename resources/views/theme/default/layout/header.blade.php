<header id="header" class="sticky">
    <div class="container">
        <div class="menu">
            <div class="logo"><a href="#wrap"> <img src="{{ url('theme/default/images/logo.png') }}" alt=""> </a></div>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1"><span class="sr-only">Toggle navigation</span> <span
                            class="ion-navicon-round"></span></button>
            </div>

            <!--======= NAV START =========-->
            <nav class="navbar navbar-default" role="navigation">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->

                    <div class="container-fluid">
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <?php $menus = \App\Models\Menu::getMenu('frontend'); ?>
                                @foreach($menus as $menu)
                                    <?php
                                    if (Session::get('lang')):
                                        $folder = Session::get('lang');
                                        $lang = SiteHelper::langOption();
                                        $menu_lang = json_decode($menu->menu_lang);
                                        $name = $menu_lang->title->$folder;
                                    else:
                                        $name = $menu->menu_name;
                                    endif;
                                    ?>
                                    <li>
                                        <a href="{{ url($menu->slug) }}"><span class="text-white">{{ $name }}</span></a>
                                    </li>

                                @endforeach
                                @if( \Auth::check())
                                    <li>
                                        <a id="dLabel" data-toggle="dropdown" aria-haspopup="true"
                                           aria-expanded="false">
                                                <span class="text-white">Hello {{ \Auth::user()->name }} &nbsp;&nbsp;<i
                                                            class="caret"></i></span>
                                        </a>
                                        <ul class="dropdown-menu nav navbar-nav"
                                            style="background: #212121;width: 100%;padding: 0 20px;border-radius: 4px;"
                                            aria-labelledby="dLabel">
                                            <li style="width: 100%;">
                                                <a href="{{ url('member/dashboard') }}"><span class="text-white">Dashboard</span>&nbsp;&nbsp;<span style="background: #FFF;padding: 3px;border-radius: 20%;color: #000">14</span></a>
                                            </li>
                                            <li style="width: 100%;">
                                                <a href="{{ url('logout') }}"><span class="text-white">Logout</span></a>
                                            </li>
                                        </ul>
                                    </li>
                                @else
                                    <li>
                                        <a href="{{ url('login') }}"><span
                                                    class="text-white">Login | Register</span></a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
            </nav>
        </div>
    </div>
</header>