<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>EventGo One Page Event Landing Page</title>
    <meta name="keywords" content="HTML5,CSS3,HTML,Template,Event Template,Freelancer Events,Awsome Event Template,Events Theme Template, One Page Events" >
    <meta name="description" content="EventGo One Page Event Landing Page" >
    <meta name="author" content="M_Adnan">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--Fonts Online-->
    <link href='//fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/default/libraries/Font-Awesome/css/font-awesome.min.css') }}">
    <!--Main-->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/default/rs-plugin/css/settings.css') }}" media="screen" >
    <link href="{{ asset('theme/default/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme/default/css/responsive.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" id="color" href="{{ asset('theme/default/css/colors/default.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/default/calendar/calendar.css') }}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if IE]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="{{ asset('theme/default/libraries/sweetalert2/dist/sweetalert2.min.css') }}">

</head>
<body>
<div class="color-switcher" id="choose_color"> <a href="#." class="picker_close"><i class="ion-settings"></i></a>
    <div class="theme-colours">
        <p>Choose Colour style</p>
        <ul>
            <li><a href="#." class="blue" id="blue"></a></li>
            <li><a href="#." class="green" id="green"></a></li>
            <li><a href="#." class="red" id="red"></a></li>
            <li><a href="#." class="yellow" id="yellow"></a></li>
            <li><a href="#." class="brown" id="brown"></a></li>
            <li><a href="#." class="cyan" id="cyan"></a></li>
            <li><a href="#." class="purple" id="purple"></a></li>
            <li><a href="#." class="sky-blue" id="sky-blue"></a></li>
            <li><a href="#." class="gray" id="gray"></a></li>
            <li><a href="#." class="black" id="black"></a></li>
            <li style="width:100%;"><a href="#." class="default" id="default" style="width:100%; text-align:center; float:left; font-weight:800; margin-top:20px;">Default</a> </li>
            <li style="width:100%;"><a href="#" style="width:100%; text-align:center; float:left; font-weight:800; margin-top:20px;"> Simple Slider</a> </li>
            <li style="width:100%;"><a href="#" style="width:100%; text-align:center; float:left; font-weight:800; margin-
      top:20px;">Revolution Slider</a> </li>

        </ul>
    </div>
</div>

<!--LOADER===========================================-->
<div id="page-loader">
  <div class="loader7"><img src="{{ url('theme/default/images/Preloader.gif') }}" alt="">
  <h1>Loading.....</h1>
  </div>
</div>

<!-- Page Wrap ===========================================-->
<div id="wrap">

    @include('theme.default.layout.header')

    @yield('content')

</div>
<script src="{{ asset('theme/default/js/jquery-1.9.1.min.js') }}"></script>
<script src="{{ asset('theme/default/js/wow.min.js') }}"></script>
<script src="{{ asset('theme/default/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('theme/default/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('theme/default/js/jquery.prettyPhoto.js') }}"></script>
<script src="{{ asset('theme/default/js/smooth-scroll.js') }}"></script>
<script src="{{ asset('theme/default/js/jquery.sticky.js') }}"></script>
<script src="{{ asset('theme/default/js/jquery.easing.1.3.js') }}"></script>
<script src="{{ asset('theme/default/js/jquery.downCount.js') }}"></script>

<script type="text/javascript" src="{{ asset('theme/default/rs-plugin/js/jquery.themepunch.plugins.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('theme/default/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{ asset('theme/default/js/color-switcher.js') }}"></script>
<script src="{{ asset('theme/default/js/main.js') }}"></script>
<script type="text/javascript" src="{{ asset('theme/default/calendar/calendar.js') }}"></script>

<script src="{{ asset('theme/default/libraries/sweetalert2/dist/sweetalert2.min.js') }}"></script>
<script src="{{ asset('theme/default/libraries/es6-promise/promise.min.js') }}"></script>
@yield('script')

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

</body>

</html>